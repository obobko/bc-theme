<?php
/* Template Name: Subscribe */

get_header(); ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>
				
				<div class="mailchform">
				<?php echo htmlspecialchars_decode(get_option('substxtbox3')); ?>
			</div>

				<?php /* Add comments */  consulting_thinkup_input_allowcomments(); ?>

			<?php endwhile; ?>
			

<?php get_footer(); ?>