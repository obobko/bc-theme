<?php
/**
 * Setup theme functions for Consulting.
 *
 * @package ThinkUpThemes
 */

// Declare latest theme version
$GLOBALS['consulting_thinkup_theme_version'] = '1.0.7';

function countPostByTermId($term_id)
{
	global $wpdb;
	$sql = 'SELECT count(*) AS cnt FROM `bc_term_relationships` WHERE term_taxonomy_id='.intval($term_id);
	$res = $wpdb->get_results($sql,ARRAY_A);
	$cnt = $res[0]['cnt'];
	
	return $cnt;
}

function ST4_columns_head($defaults) {
    $defaults['price'] = 'Price';
	$defaults['price_sales'] = 'Sales';
    return $defaults;
}
 
// SHOW THE FEATURED IMAGE
function ST4_columns_content($column_name, $post_ID) {
    if ($column_name == 'price') {
        $price = get_field('price', $post_ID);
       
            echo '<input type="text" class="pricef" data-rel="'.$post_ID.'" value="' . $price . '" />';
        }
		
	if ($column_name == 'price_sales') {
        $price_sales = get_field('price_sales', $post_ID);
       
            echo '<input type="text" class="price_salesf" data-rel="'.$post_ID.'" value="' . $price_sales . '" />';
        }
}

if(isset($_GET['post_type']) && ($_GET['post_type'] == 'product'))
{
	add_filter('manage_posts_columns', 'ST4_columns_head');
	add_action('manage_posts_custom_column', 'ST4_columns_content', 10, 2);
}
/********AJAX****************/

function itemnumclean_script( $hook ) {
    if ( 'post.php' != $hook ) {
        return;
    }
    wp_enqueue_script( 'itemnumclean', get_template_directory_uri().'/itemnumclean.js', array(), '1.0' );
	wp_enqueue_script( 'editprice', get_template_directory_uri().'/editprice.js', array(), '1.0' );
}

function mygrid_script( $hook ) {
    if ( 'edit.php' != $hook ) {
        return;
    }
	wp_enqueue_script( 'editprice', get_template_directory_uri().'/editprice.js', array(), '1.0' );
}

add_action( 'admin_enqueue_scripts', 'itemnumclean_script' );
add_action( 'admin_enqueue_scripts', 'mygrid_script' );

add_action( 'wp_ajax_my_action', 'my_action_callback' );
add_action('wp_ajax_nopriv_my_action', 'my_action_callback');

add_action( 'wp_ajax_cartup', 'cartup_callback' );
add_action('wp_ajax_nopriv_cartup', 'cartup_callback');

add_action( 'wp_ajax_counup', 'counup_callback' );
add_action('wp_ajax_nopriv_counup', 'counup_callback');

add_action( 'wp_ajax_shipup', 'shipup_callback' );
add_action('wp_ajax_nopriv_shipup', 'shipup_callback');

add_action( 'wp_ajax_getcartbut', 'getcartbut_callback' );
add_action('wp_ajax_nopriv_getcartbut', 'getcartbut_callback');

add_action( 'wp_ajax_getbuybutt', 'getbuybutt_callback' );
add_action('wp_ajax_nopriv_getbuybutt', 'getbuybutt_callback');

add_action( 'wp_ajax_getshipmentdrop', 'getshipmentdrop_callback' );
add_action('wp_ajax_nopriv_getshipmentdrop', 'getshipmentdrop_callback');

add_action( 'wp_ajax_editprice', 'editprice_callback' );
add_action('wp_ajax_nopriv_editprice', 'editprice_callback');

add_action( 'wp_ajax_editpricesales', 'editpricesales_callback' );
add_action('wp_ajax_nopriv_editpricesales', 'editpricesales_callback');

add_action( 'wp_ajax_newshipper', 'newshipper_callback' );
add_action('wp_ajax_nopriv_newshipper', 'newshipper_callback');

add_action( 'wp_ajax_delshipper', 'delshipper_callback' );
add_action('wp_ajax_nopriv_delshipper', 'delshipper_callback');

add_action( 'wp_ajax_editshipper', 'editshipper_callback' );
add_action('wp_ajax_nopriv_editshipper', 'editshipper_callback');

function editshipper_callback()
{
	global $wpdb;
	
	if(!empty($_POST))
		$data = $_POST;
	elseif(!empty($_GET))
		$data = $_GET;
	else
	{
		echo json_encode(array('result' => false, 'message' => 'No data provided'));
		die();
	}
	
	if(isset($data['val']) && ($data['relname'] == 'isdefault') && ($data['val'] == 'on'))
	{
		$wpdb->query('UPDATE shippers SET isdefault=0');
		$data['val'] = 1;
	}
	
	if($data['relname'] == 'active')
	{
		$active = $wpdb->get_results('SELECT active FROM shippers WHERE id='.intval($data['id']), ARRAY_A);
		$active = $active[0]['active'];
		if(intval($active))
			$data['val'] = 0;
		else
			$data['val'] = 1;
	}
	
	$ifupdate = $wpdb->update('shippers', array($data['relname'] => $data['val']), array('id' => $data['id']));
	
	if($ifupdate)
		echo json_encode(array('result' => true, 'message' => 'success'));
	else
		echo json_encode(array('result' => false, 'message' => 'error'));
	
	wp_die();
}


function newshipper_callback()
{
	if(!empty($_POST))
		$data = $_POST;
	elseif(!empty($_GET))
		$data = $_GET;
	else
	{
		echo json_encode(array('result' => false, 'message' => 'No data provided'));
		die();
	}
	
	global $wpdb;
	
	$intoShippersTable = $wpdb->insert('shippers', array('id' => null, 'pseudo' => $data['pseudo'], 'name' => $data['name'], 'prices_table' => $data['prices_table'], 'restrict_cntrs' => $data['restrict_cntrs'], 'altershiptime' => $data['altershiptime'], 'isdefault' => 0, 'freeonlimit' => $data['freeonlimit'], 'freeforzone' => $data['freeforzone'], 'level' => $data['level'], 'active' => 1));
	
	$checkSpeed = $wpdb->get_results('SELECT * FROM speed LIMIT 0,1', ARRAY_A);
	$checkSpeed = $checkSpeed[0];
	$checkSpeed = array_keys($checkSpeed);
	$lastSpeed = end($checkSpeed);
		
	$alterSpeed = $wpdb->query('ALTER TABLE `speed` ADD `'.$data['pseudo'].'` VARCHAR(100) NOT NULL AFTER `'.$lastSpeed.'`');
	
	if(!isset($data['prices_table_orig']))
	{
		$prices_table_orig = $wpdb->get_results('SELECT pseudo, prices_table FROM shippers WHERE isdefault=1', ARRAY_A);
		$data['prices_table_orig'] = $prices_table_orig[0]['prices_table'];
		$data['pseudo_orig'] = $prices_table_orig[0]['pseudo'];
	}
	
	$speedValues = $wpdb->get_results('SELECT id, '.$data['pseudo_orig'].' FROM speed', ARRAY_A);
	
	foreach($speedValues as $value)
	{
		$wpdb->query('UPDATE speed SET '.$data['pseudo'].'="'.$value[$data['pseudo_orig']].'" WHERE id='.$value['id']);
	}
	
	$newShipperTable = $wpdb->query('CREATE TABLE '.$data['prices_table'].' LIKE '.$data['prices_table_orig']);
	$wpdb->query('INSERT INTO '.$data['prices_table'].' SELECT * FROM '.$data['prices_table_orig']);
	
	if($intoShippersTable)
		echo json_encode(array('result' => true, 'message' => 'success'));
	else
		echo json_encode(array('result' => false, 'message' => 'error'));
	die();
}

function delshipper_callback()
{
	if(!empty($_POST))
		$data = $_POST;
	elseif(!empty($_GET))
		$data = $_GET;
	else
	{
		echo json_encode(array('result' => false, 'message' => 'No data provided'));
		die();
	}
	
	global $wpdb;
	
	$shipper = $wpdb->get_results('SELECT pseudo, prices_table FROM shippers WHERE id='.intval($data['idel']), ARRAY_A);
	$pseudo = $shipper[0]['pseudo'];
	$prices_table = $shipper[0]['prices_table'];
	
	$delShippersFromTable = $wpdb->query('DELETE FROM shippers WHERE id='.intval($data['idel']));
	
	$wpdb->query('ALTER TABLE `speed` DROP '.$pseudo);
	
	$wpdb->query('DROP TABLE '.$prices_table);
	
	if($delShippersFromTable)
		echo json_encode(array('result' => true, 'message' => 'success'));
	else
		echo json_encode(array('result' => false, 'message' => 'error'));
	die();
}

function editprice_callback()
{
	if(!empty($_POST))
		$data = $_POST;
	elseif(!empty($_GET))
		$data = $_GET;
	else
	{
		echo json_encode(array('result' => false, 'message' => 'No data provided'));
		die();
	}
	
	$result = update_field('price', $data['val'], $data['id']);
	if($result)
		echo json_encode(array('result' => true, 'message' => 'success'));
	else
		echo json_encode(array('result' => false, 'message' => 'error'));
	die();
}

function editpricesales_callback()
{
	if(!empty($_POST))
		$data = $_POST;
	elseif(!empty($_GET))
		$data = $_GET;
	else
	{
		echo json_encode(array('result' => false, 'message' => 'No data provided'));
		die();
	}
	
	$result = update_field('price_sales', $data['val'], $data['id']);
	
	if($result)
		echo json_encode(array('result' => true, 'message' => 'success'));
	else
		echo json_encode(array('result' => false, 'message' => 'error'));
	die();
}

function getcartbut_callback()
{
	echo do_shortcode('[wp_compact_cart]');
	wp_die();
}

function checkRegion($countryCode)
{
	global $wpdb;
	$result = $wpdb->get_results('SELECT region FROM countries WHERE iso2="'.strtolower($countryCode).'"', ARRAY_A);
	if(isset($result[0]['region']) && !empty($result[0]['region']))
		$region = $result[0]['region'];
	else
		$region = '';
	
	return $region;
}

function counup_callback()
{
	$countryCode = addslashes($_POST['countryCode']);
	$_SESSION['countryCode'] = $countryCode;
	echo json_encode(array('result' => true));
	wp_die();
}

function shipup_callback()
{
	$ship_mode = $_POST['ship_mode'];
	$_SESSION['ship_mode'] = $ship_mode;
	echo json_encode(array('result' => true));
	wp_die();
}

function my_action_callback() {
	$post_id = intval($_POST['item_name']);
	$email = addslashes($_POST['email']);
	$first_name = addslashes($_POST['first_name']);
	$last_name = addslashes($_POST['last_name']);
	$userData = array('user_login'=>$email, 'username'=>$email, 'user_pass' => md5($email), 'user_email'=>$email, 'first_name'=>$first_name, 'last_name'=>$last_name);
	$userid = wp_insert_user($userData);
	update_user_meta( $userid, 'address1', addslashes($_POST['address1']) );
	update_user_meta( $userid, 'address2', addslashes($_POST['address2']) );
	update_user_meta( $userid, 'city', addslashes($_POST['city']) );
	update_user_meta( $userid, 'country', addslashes($_POST['country']) );
	update_user_meta( $userid, 'state', addslashes($_POST['state']) );
	update_user_meta( $userid, 'zip', addslashes($_POST['zip']) );
	update_post_meta($post_id, 'wpspsc_phone', addslashes($_POST['phone']));
	
	$cart = stripslashes($_POST['cart']);
		
	update_post_meta($post_id, 'wpsc_total_amount', addslashes($_POST['amount']));
	update_post_meta($post_id, 'wpsc_subtotal_amount', addslashes($_POST['sub_amount']));
	update_post_meta($post_id, 'wpsc_shipping_amount', addslashes($_POST['shipping_amount']));
	update_post_meta($post_id, 'wpsc_ipaddress', addslashes($_POST['ip']));
	update_post_meta($post_id, 'wpsc_sh_option', addslashes($_POST['sh_option']));
	update_post_meta($post_id, 'wpsc_first_name', addslashes($first_name));
	update_post_meta($post_id, 'wpsc_last_name', addslashes($last_name));
	update_post_meta($post_id, 'wpsc_email_address', addslashes($email));
	update_post_meta($post_id, 'wpspsc_items_ordered', addslashes($cart));
	update_post_meta($post_id, 'wpsc_status', 'local pending');
	update_post_meta($post_id, 'wpsc_address1', addslashes($_POST['address1']));
	update_post_meta($post_id, 'wpsc_address2', addslashes($_POST['address2']));
	update_post_meta($post_id, 'wpsc_city', addslashes($_POST['city']));
	update_post_meta($post_id, 'wpsc_country', addslashes($_POST['country']));
	update_post_meta($post_id, 'wpsc_state', addslashes($_POST['state']));
	update_post_meta($post_id, 'wpsc_zip', addslashes($_POST['zip']));
	update_post_meta($post_id, 'coupon', addslashes($_POST['coupon']));
	
	//unset($_SESSION['simpleCart']);
	//unset($_SESSION['order_id']);

	wp_die();
}
/****************************/

function cartup_callback()
{
	$item = addslashes($_POST['item']);
	$q = addslashes($_POST['q']);
	$cart = $_SESSION['simpleCart'];
	$numbers_col = array_column($cart, 'item_number');
	$item_index = array_search($item, $numbers_col);
	$cart[$item_index]['quantity'] = $q;
	$_SESSION['simpleCart'] = $cart;
	wp_die();
}

/******User custom fields**********/

/********End of user custom fields********/

// Setup content width 
function consulting_thinkup_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'consulting_thinkup_content_width', 1170 );
}
add_action( 'after_setup_theme', 'consulting_thinkup_content_width', 0 );

function polylang_shortcode() {
	ob_start();
	pll_the_languages(array('show_flags'=>1,'show_names'=>0));
	$flags = ob_get_clean();
	return $flags;
}
add_shortcode( 'polylang', 'polylang_shortcode' );

pll_register_string('new_goods', 'news');
//----------------------------------------------------------------------------------
//	Add Theme Options Panel & Assign Variable Values
//----------------------------------------------------------------------------------

	// Add Cusomizer Framework
	require_once( get_template_directory() . '/admin/main/framework.php' );
	require_once( get_template_directory() . '/admin/main/options.php' );

	// Add Toolbox Framework
	require_once( get_template_directory() . '/admin/main-toolbox/toolbox.php' );

	// Add Theme Options Features.
	require_once( get_template_directory() . '/admin/main/options/00.theme-setup.php' ); 
	require_once( get_template_directory() . '/admin/main/options/01.general-settings.php' ); 
	require_once( get_template_directory() . '/admin/main/options/02.homepage.php' ); 
	require_once( get_template_directory() . '/admin/main/options/03.header.php' ); 
	require_once( get_template_directory() . '/admin/main/options/04.footer.php' );
	require_once( get_template_directory() . '/admin/main/options/05.blog.php' ); 
	
	
	add_action('admin_init', 'my_general_section');  
function my_general_section() {  
    add_settings_section(  
        'my_settings_section', // Section ID 
        'Налаштування каталогу', // Section Title
        'my_section_options_callback', // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

    add_settings_field( // Option 1
        'option_1', // Option ID
        'Укр.', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'option_1' // Should match Option ID
        )  
    ); 

    add_settings_field( // Option 2
        'option_2', // Option ID
        'Eng.', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_settings_section', // Name of our section (General Settings)
        array( // The $args
            'option_2' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'ms_agent', // Option ID
        'MS Agent', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'ms_agent' // Should match Option ID
        )  
    );
	
	add_settings_field( // Option 1
        'ms_org', // Option ID
        'MS Organization', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'ms_org' // Should match Option ID
        )  
    );
	
	add_settings_field( // Option 1
        'ms_proj', // Option ID
        'MS Project', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'ms_proj' // Should match Option ID
        )  
    );
	
	add_settings_field( // Option 1
        'ms_store', // Option ID
        'MS Store', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'ms_store' // Should match Option ID
        )  
    );
	
	add_settings_field( // Option 1
        'ms_state', // Option ID
        'MS Status', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'ms_state' // Should match Option ID
        )  
    );
	
	add_settings_field( // Option 1
        'ms_rate', // Option ID
        'MS Currency', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'ms_rate' // Should match Option ID
        )  
    );
	
	/*add_settings_field( // Option 1
        'shipser1', // Option ID
        'Служба доставки 1', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'shipser1' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'shipsertable1', // Option ID
        'Таблица служба доставки 1', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'shipsertable1' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'shipser2', // Option ID
        'Служба доставки 2', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'shipser2' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'shipsertable2', // Option ID
        'Таблица служба доставки 2', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'shipsertable2' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'shipser3', // Option ID
        'Служба доставки 3', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'shipser3' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'shipsertable3', // Option ID
        'Таблица служба доставки 3', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'shipsertable3' // Should match Option ID
        )  
    ); */
	
	add_settings_field( // Option 1
        'substxtbox', // Option ID
        'Форма підписки на головній', // Label
        'my_textbox_callback2', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'substxtbox' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'substxtbox2', // Option ID
        'Форма підписки в блозі', // Label
        'my_textbox_callback2', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'substxtbox2' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'substxtbox3', // Option ID
        'Форма підписки на сторінці підписки', // Label
        'my_textbox_callback2', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'substxtbox3' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'substxtbox4', // Option ID
        'Форма підписки в листі покупцям', // Label
        'my_textbox_callback2', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'substxtbox4' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'buyamazon', // Option ID
        'Buy on wherever text', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'buyamazon' // Should match Option ID
        )  
    ); 
	
	add_settings_field( // Option 1
        'amazonctrs', // Option ID
        'Buy wherever countries (ISO2 codes, coma separated, no spaces)', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'amazonctrs' // Should match Option ID
        )  
    ); 
	
	/*add_settings_field( // Option 1
        'freeshiplim', // Option ID
        'Free shipping limit', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'freeshiplim' // Should match Option ID
        )  
    );*/ 

    register_setting('general','option_1', 'esc_attr');
    register_setting('general','option_2', 'esc_attr');
	//register_setting('general','shipser1', 'esc_attr');
	//register_setting('general','shipsertable1', 'esc_attr');
    //register_setting('general','shipser2', 'esc_attr');
	//register_setting('general','shipsertable2', 'esc_attr');
	//register_setting('general','shipser3', 'esc_attr');
	//register_setting('general','shipsertable3', 'esc_attr');
	register_setting('general','substxtbox', 'esc_attr');
	register_setting('general','substxtbox2', 'esc_attr');
	register_setting('general','substxtbox3', 'esc_attr');
	register_setting('general','substxtbox4', 'esc_attr');
	register_setting('general','buyamazon', 'esc_attr');
	register_setting('general','amazonctrs', 'esc_attr');
	//register_setting('general','freeshiplim', 'esc_attr');
	register_setting('general','ms_agent', 'esc_attr');
	register_setting('general','ms_org', 'esc_attr');
	register_setting('general','ms_proj', 'esc_attr');
	register_setting('general','ms_store', 'esc_attr');
	register_setting('general','ms_state', 'esc_attr');
	register_setting('general','ms_rate', 'esc_attr');
}

function my_section_options_callback() { // Section Callback
    echo '<p>Введість ID категорій каталогу продуктів</p>';  
}

function my_textbox_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
}
	
function my_textbox_callback2($args) {  // Textbox Callback
    $option = get_option($args[0]);
    echo '<textarea id="'. $args[0] .'" name="'. $args[0] .'" rows="20" cols="30">' . $option . '</textarea>';
}

function getAmazonctrs()
{
	$amazonctrs = get_option('amazonctrs');
	if(empty($amazonctrs))
		return false;
	
	if(stripos($amazonctrs, ','))
		return explode(',', $amazonctrs);
	
	return array($amazonctrs);
}

//----------------------------------------------------------------------------------
//	Assign Theme Specific Functions
//----------------------------------------------------------------------------------

// Setup theme features, register menus and scripts.
if ( ! function_exists( 'consulting_thinkup_themesetup' ) ) {

	function consulting_thinkup_themesetup() {

		// Load required files
		require_once ( get_template_directory() . '/lib/functions/extras.php' );
		require_once ( get_template_directory() . '/lib/functions/template-tags.php' );

		// Make theme translation ready.
		load_theme_textdomain( 'consulting', get_template_directory() . '/languages' );

		// Add default theme functions.
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'post-formats', array( 'image' ) );
		add_theme_support( 'title-tag' );
		// Add support for custom background
		add_theme_support( 'custom-background' );

		// Add support for custom header
		$consulting_thinkup_header_args = apply_filters( 'consulting_thinkup_custom_header', array( 'height' => 200, 'width'  => 1600, 'header-text' => false, 'flex-height' => true ) );
		add_theme_support( 'custom-header', $consulting_thinkup_header_args );

		// Add support for custom logo
		add_theme_support( 'custom-logo', array( 'height' => 90, 'width' => 200, 'flex-width' => true, 'flex-height' => true ) );

		// Add WooCommerce functions.
		add_theme_support( 'woocommerce' );

		// Add excerpt support to pages.
		add_post_type_support( 'page', 'excerpt' );

		// Register theme menu's.
		register_nav_menus( array( 'pre_header_menu' => __( 'Pre Header Menu', 'consulting' ) ) );
		register_nav_menus( array( 'header_menu'     => __( 'Primary Header Menu', 'consulting' ) ) );
		register_nav_menus( array( 'sub_footer_menu' => __( 'Footer Menu', 'consulting' ) ) );
	}
}
add_action( 'after_setup_theme', 'consulting_thinkup_themesetup' );


//----------------------------------------------------------------------------------
//	Register Front-End Styles And Scripts
//----------------------------------------------------------------------------------

function consulting_thinkup_frontscripts() {

	global $consulting_thinkup_theme_version;

	// Add 3rd party stylesheets
	wp_enqueue_style( 'prettyPhoto', get_template_directory_uri() . '/lib/extentions/prettyPhoto/css/prettyPhoto.css', '', '3.1.6' );

	// Add 3rd party stylesheets - Prefixed to prevent conflict between library versions
	wp_enqueue_style( 'consulting-thinkup-bootstrap', get_template_directory_uri() . '/lib/extentions/bootstrap/css/bootstrap.min.css', '', '2.3.2' );

	// Add 3rd party Font Packages
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/lib/extentions/font-awesome/css/font-awesome.min.css', '', '4.7.0' );

	// Add 3rd party scripts
	wp_enqueue_script( 'imagesloaded' );
	wp_enqueue_script( 'prettyPhoto', ( get_template_directory_uri().'/lib/extentions/prettyPhoto/js/jquery.prettyPhoto.js' ), array( 'jquery' ), '3.1.6', 'true' );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/lib/scripts/modernizr.js', array( 'jquery' ), '2.6.2', 'true'  );
	wp_enqueue_script( 'sticky', get_template_directory_uri() . '/lib/scripts/plugins/sticky/jquery.sticky.js', '1.0.0', 'true' );
	wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/lib/scripts/plugins/waypoints/waypoints.min.js', array( 'jquery' ), '2.0.3', 'true'  );
	wp_enqueue_script( 'waypoints-sticky', get_template_directory_uri() . '/lib/scripts/plugins/waypoints/waypoints-sticky.min.js', array( 'jquery' ), '2.0.3', 'true'  );	
	wp_enqueue_script( 'jquery-scrollup', get_template_directory_uri() . '/lib/scripts/plugins/scrollup/jquery.scrollUp.min.js', array( 'jquery' ), '2.4.1', 'true' );

	// Add 3rd party scripts - Prefixed to prevent conflict between library versions
	wp_enqueue_script( 'consulting-thinkup-bootstrap', get_template_directory_uri() . '/lib/extentions/bootstrap/js/bootstrap.js', array( 'jquery' ), '2.3.2', 'true' );

	// Register 3rd party scripts

	// Add theme stylesheets
	wp_enqueue_style( 'consulting-thinkup-shortcodes', get_template_directory_uri() . '/styles/style-shortcodes.css', '', $consulting_thinkup_theme_version );
	wp_enqueue_style( 'consulting-thinkup-style', get_stylesheet_uri(), '', $consulting_thinkup_theme_version );

	// Add theme scripts
	wp_enqueue_script( 'consulting-thinkup-frontend', get_template_directory_uri() . '/lib/scripts/main-frontend.js', array( 'jquery' ), $consulting_thinkup_theme_version, 'true' );

	// Register theme stylesheets
	wp_register_style( 'consulting-thinkup-responsive', get_template_directory_uri() . '/styles/style-responsive.css', '', $consulting_thinkup_theme_version );
	wp_register_style( 'consulting-thinkup-sidebarleft', get_template_directory_uri() . '/styles/layouts/thinkup-left-sidebar.css', '', $consulting_thinkup_theme_version );
	wp_register_style( 'consulting-thinkup-sidebarright', get_template_directory_uri() . '/styles/layouts/thinkup-right-sidebar.css', '', $consulting_thinkup_theme_version );

	// Register WooCommerce (theme specific) stylesheets

	// Add Masonry script to all archive pages
	if ( consulting_thinkup_check_isblog() or is_archive() ) {
		wp_enqueue_script( 'jquery-masonry' );
	}

	// Add Portfolio styles & scripts

	// Add ThinkUpSlider scripts
	if ( is_front_page() ) {
		wp_enqueue_script( 'responsiveslides', get_template_directory_uri() . '/lib/scripts/plugins/ResponsiveSlides/responsiveslides.min.js', array( 'jquery' ), '1.54', 'true' );
		wp_enqueue_script( 'consulting-thinkup-responsiveslides', get_template_directory_uri() . '/lib/scripts/plugins/ResponsiveSlides/responsiveslides-call.js', array( 'jquery' ), $consulting_thinkup_theme_version, 'true' );
	}

	// Add comments reply script
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'consulting_thinkup_frontscripts', 10 );


//----------------------------------------------------------------------------------
//	Register Back-End Styles And Scripts
//----------------------------------------------------------------------------------

function consulting_thinkup_adminscripts() {

	if ( is_customize_preview() ) {

		global $consulting_thinkup_theme_version;

		// Add theme stylesheets
		wp_enqueue_style( 'consulting-thinkup-backend', get_template_directory_uri() . '/styles/backend/style-backend.css', '', $consulting_thinkup_theme_version );
		wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/lib/extentions/font-awesome/css/font-awesome.min.css', '', '4.7.0' );

		// Add theme scripts
		wp_enqueue_script( 'consulting-thinkup-backend', get_template_directory_uri() . '/lib/scripts/main-backend.js', array( 'jquery' ), $consulting_thinkup_theme_version );

	}
}
add_action( 'customize_controls_enqueue_scripts', 'consulting_thinkup_adminscripts' );


//----------------------------------------------------------------------------------
//	Register Theme Widgets
//----------------------------------------------------------------------------------

function consulting_thinkup_widgets_init() {
	// Register default sidebar
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'consulting' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	// Register footer sidebars
    register_sidebar( array(
        'name'          => __( 'Footer Column 1', 'consulting' ),
        'id'            => 'footer-w1',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="footer-widget-title"><span>',
        'after_title'   => '</span></h3>',
    ) );
 
    register_sidebar( array(
        'name'          => __( 'Footer Column 2', 'consulting' ),
        'id'            => 'footer-w2',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="footer-widget-title"><span>',
        'after_title'   => '</span></h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer Column 3', 'consulting' ),
        'id'            => 'footer-w3',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="footer-widget-title"><span>',
        'after_title'   => '</span></h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer Column 4', 'consulting' ),
        'id'            => 'footer-w4',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="footer-widget-title"><span>',
        'after_title'   => '</span></h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer Column 5', 'consulting' ),
        'id'            => 'footer-w5',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="footer-widget-title"><span>',
        'after_title'   => '</span></h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer Column 6', 'consulting' ),
        'id'            => 'footer-w6',
        'before_widget' => '<aside class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="footer-widget-title"><span>',
        'after_title'   => '</span></h3>',
    ) );
}
add_action( 'widgets_init', 'consulting_thinkup_widgets_init' );

add_image_size( 'underb', 450, 300, true );
add_image_size( 'gal', 270, 270, true );

/*************Product**************/

function product_post_type() {
	$labels = array(        
	'name'                => _x( 'Product', 'Post Type General Name', 'twentythirteen' ),        
	
	'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'twentythirteen' ),        
	
	'menu_name'           => __( 'Products', 'twentythirteen' ),        
	
	'parent_item_colon'   => __( 'Parent Product', 'twentythirteen' ),        
	
	'all_items'           => __( 'All Products', 'twentythirteen' ),        
	
	'view_item'           => __( 'View Product', 'twentythirteen' ),        
	
	'add_new_item'        => __( 'Add New Product', 'twentythirteen' ),        
	
	'add_new'             => __( 'Add New', 'twentythirteen' ),        
	
	'edit_item'           => __( 'Edit Product', 'twentythirteen' ),        
	
	'update_item'         => __( 'Update Product', 'twentythirteen' ),        
	
	'search_items'        => __( 'Search Product', 'twentythirteen' ),        
	
	'not_found'           => __( 'Not Found', 'twentythirteen' ),        
	
	'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),    );              
	
	$args = array(        'label'               => __( 'product', 'twentythirteen' ),        
	
	'description'         => __( 'Product', 'twentythirteen' ),        
	
	'labels'              => $labels,        
	
	'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields'),		
	
	'taxonomies'			=> array( 'category' ),        
	
	'hierarchical'        => false,        
	
	'public'              => true,        
	
	'show_ui'             => true,        
	
	'show_in_menu'        => true,        
	
	'show_in_nav_menus'   => true,        
	
	'show_in_admin_bar'   => true,       

	'menu_position'       => 5,        'can_export'          => true,        
	
	'has_archive'         => true,        'exclude_from_search' => false,        
	
	'publicly_queryable'  => true,       

	'capability_type'     => 'post'    );         
	
	register_post_type( 'product', $args ); 
	
	}  
	
	add_action( 'init', 'product_post_type', 0 );
	
	/*************Galleries**************/

function gallery_post_type() {
	$labels = array(        
	'name'                => _x( 'Galleries', 'Post Type General Name', 'twentythirteen' ),        
	
	'singular_name'       => _x( 'Gallery item', 'Post Type Singular Name', 'twentythirteen' ),        
	
	'menu_name'           => __( 'Gallery items', 'twentythirteen' ),        
	
	'parent_item_colon'   => __( 'Parent gallery item', 'twentythirteen' ),        
	
	'all_items'           => __( 'All gallery items', 'twentythirteen' ),        
	
	'view_item'           => __( 'View gallery items', 'twentythirteen' ),        
	
	'add_new_item'        => __( 'Add New gallery item', 'twentythirteen' ),        
	
	'add_new'             => __( 'Add New', 'twentythirteen' ),        
	
	'edit_item'           => __( 'Edit gallery item', 'twentythirteen' ),        
	
	'update_item'         => __( 'Update gallery item', 'twentythirteen' ),        
	
	'search_items'        => __( 'Search gallery item', 'twentythirteen' ),        
	
	'not_found'           => __( 'Not Found', 'twentythirteen' ),        
	
	'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),    );              
	
	$args = array(        'label'               => __( 'gallery item', 'twentythirteen' ),        
	
	'description'         => __( 'Gallery items', 'twentythirteen' ),        
	
	'labels'              => $labels,        
	
	'supports'            => array( 'title', 'thumbnail', 'custom-fields'),		
	
	'taxonomies'			=> array( 'category' ),        
	
	'hierarchical'        => false,        
	
	'public'              => true,        
	
	'show_ui'             => true,        
	
	'show_in_menu'        => true,        
	
	'show_in_nav_menus'   => true,        
	
	'show_in_admin_bar'   => true,       

	'menu_position'       => 5,        'can_export'          => true,        
	
	'has_archive'         => true,        'exclude_from_search' => false,        
	
	'publicly_queryable'  => true,       

	'capability_type'     => 'post'    );         
	
	register_post_type( 'gallery', $args ); 
	
	}  
	
	add_action( 'init', 'gallery_post_type', 0 );
	
	
	if(function_exists('add_db_table_editor')){

global $wpdb;
$shipTables = $wpdb->get_results('SELECT pseudo, prices_table FROM shippers', ARRAY_A);

foreach($shipTables as $st)
{
	add_db_table_editor(array(
	'title'=>ucfirst($st['pseudo']).' table',
	'table'=>$st['prices_table'],
	'sql'=>'SELECT * FROM '.$st['prices_table'].' ORDER BY weight ASC',
	'noedit'=>false
	));
}

add_db_table_editor(array(
'title'=>'Countries',
'table'=>'countries',
'sql'=>'SELECT * FROM countries ORDER BY id ASC',
'noedit'=>false
));

add_db_table_editor(array(
'title'=>'Speed',
'table'=>'speed',
'sql'=>'SELECT * FROM speed ORDER BY id ASC',
'noedit'=>false
));

add_db_table_editor(array(
'title'=>'Zones',
'table'=>'zones',
'sql'=>'SELECT * FROM zones ORDER BY id ASC',
'noedit'=>false
));
}

function getCountryByIP()
{
	$client = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote = @$_SERVER['REMOTE_ADDR'];
    $result = array('country' => '', 'city' => '');
    if (filter_var($client, FILTER_VALIDATE_IP)) {
      $ip = $client;
    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
      $ip = $forward;
    } else {
      $ip = $remote;
    }
	
	$data = file_get_contents('http://ip-api.com/json/'.$ip);
	
	return json_decode($data, true);
}

function generateCountryDropDown($selected = '')
{
	global $wpdb;
	
	$str = '';
	$selected = strtolower($selected);
	$results = $wpdb->get_results('SELECT * FROM countries ORDER BY id ASC');

	foreach($results as $result)
	{
		$iso2 = strtolower($result->iso2);
		if($iso2 == $selected )
			$selTxt = 'selected="selected"';
		else
			$selTxt = '';
		$str .= '<option value="'.$iso2.'" data-image="'.get_template_directory_uri().'/country-dropdown/images/msdropdown/icons/blank.gif" data-imagecss="flag '.$iso2.'" data-title="'.$result->country.'" '.$selTxt.'>'.$result->country.'</option>';
	}
	
	return $str;
}

function calculateSumPrice($p, $q)
{
	$res = 0;
	
	$cp = count($p);
	$cq = count($q);
	
	if($cp != $cq)
		return $res;
	
	for($i = 0; $i < $cp; $i++)
	{
		$res += $p[$i]*$q[$i];
	}
	
	return $res;
}

function calculateShippingPrice()
{
	global $wpdb;
	
	$shippersList = $wpdb->get_results('SELECT * FROM shippers WHERE active=1 ORDER BY level ASC', ARRAY_A);
	$shippersPseudo = array_column($shippersList, 'pseudo');
	$shippersList = array_combine($shippersPseudo, $shippersList);
	
	$cart = $_SESSION['simpleCart'];
	
	$pricesList = array_column($cart, 'price');
	$quantityList = array_column($cart, 'quantity');
	
	$sumPrice = calculateSumPrice($pricesList, $quantityList);
	
	/******Get region of shipping*****/
	if(!isset($_SESSION['countryCode']))
	{
		$userIpData = getCountryByIP(); 
		$countryCode = $userIpData['countryCode'];
		$_SESSION['countryCode'] = $countryCode;
	}
	else
	{
		$countryCode = $_SESSION['countryCode'];
	}
	
	$result = $wpdb->get_results('SELECT region FROM countries WHERE iso2="'.$countryCode.'"', ARRAY_A);
	if(isset($result[0]['region']) && !empty($result[0]['region']))
		$region = $result[0]['region'];
	else
		$region = '';
	/********************/
	$item_numbers = array_column($cart, 'item_number');
	$item_number = implode('","', $item_numbers);
	$it2pid = $wpdb->get_results('SELECT pm.post_id, pm.meta_value as item_number FROM bc_postmeta AS pm WHERE pm.meta_key="item_number" AND pm.meta_value IN ("'.$item_number.'")', ARRAY_A);
	$post_ids_i = array_column($it2pid, 'post_id');
	$item_number_i = array_column($it2pid, 'item_number');
	$post_ids_i = array_combine($item_number_i, $post_ids_i);
	/****************/
	$isFrees = $wpdb->get_results('SELECT pm.meta_value as free_shipping, pm.post_id FROM bc_postmeta As pm WHERE pm.meta_key="free_shipping" AND pm.post_id IN (SELECT pm2.post_id FROM bc_postmeta AS pm2 WHERE pm2.meta_key="item_number" AND pm2.meta_value IN ("'.$item_number.'"))', ARRAY_A);
	$post_ids_f = array_column($isFrees, 'post_id');
	$free_ship_f =  array_column($isFrees, 'free_shipping');
	foreach($free_ship_f as $key => $value)
	{
		$temp = unserialize($value);
		if(isset($temp[0]))
			$temp = $temp[0];
		else
			$temp = '';
		
		$free_ship_f[$key] = $temp;
	}
	$free_ships = array_combine($post_ids_f, $free_ship_f);
	
	$region_list = array('zone1','zone2','zone3','zone4','zone5','zone6','zone7','zone8','zone9','other');
	
	$total_ret = array();
	
	if($region != '' && in_array($region, $region_list))
	{
		$weights = $wpdb->get_results('SELECT pm.meta_value as weight, pm.post_id FROM bc_postmeta As pm WHERE pm.meta_key="weight" AND pm.post_id IN (SELECT pm2.post_id FROM bc_postmeta AS pm2 WHERE pm2.meta_key="item_number" AND pm2.meta_value IN ("'.$item_number.'"))', ARRAY_A);
		/**************/
		$post_ids_w = array_column($weights, 'post_id');
		$weight_w = array_column($weights, 'weight');
		$weights = array_combine($post_ids_w, $weight_w );
		/****************/
		$total_weight = 0;
		foreach($cart as $el)
		{
			if(isset($free_ships[$post_ids_i[$el['item_number']]]) && $free_ships[$post_ids_i[$el['item_number']]] == 'Yes')
				continue;
			$total_weight += $weights[$post_ids_i[$el['item_number']]]*$el['quantity'];
		}
				
		$total_ret = array();
		
		foreach($shippersList as $key => $value)
		{
			if($total_weight != 0)
			{
				$tempPrice = $wpdb->get_results('SELECT max(z1.'.$region.') AS p FROM '.$value['prices_table'].' AS z1 WHERE z1.weight <= '.$total_weight, ARRAY_A);
				$total_ret[$key] = $tempPrice[0]['p'];
			}
			else
				$total_ret[$key] = 0;
			
			if(($value['freeonlimit'] != 0) && ($sumPrice > $value['freeonlimit']))
				$total_ret[$key] = 0;
			
			if(!empty($value['freeforzone']))
			{
				$freeforzones = explode(',', $value['freeforzone']);
				if(in_array($region, $freeforzones))
					$total_ret[$key] = 0;
			}
		}
	}
	else
	{	
		$shipping_price = 0;

		foreach($cart as $el)
		{
			$shipping_default = get_field('shipping_default', $post_ids_i[$el['item_number']]);
			$shipping_price += $shipping_default*$el['quantity'];
		}
		
		foreach($shippersPseudo	as $shipperPseudo)
		{
			$total_ret[$shipperPseudo] = $shipping_price;
		}		
	}
	
	return $total_ret;
}

function printShPrice($input)
{
	$input = trim($input);
	if(intval($input) == 0 || empty($input))
		return 'Free shipping';
	else
		return '$ '.$input.' USD';
}

function checkIfNoGoZone($countryCode)
{
	global $wpdb;
	$sql = 'SELECT nogo FROM countries WHERE iso2="'.$countryCode.'"';
	$ifnogo = $wpdb->get_results($sql, ARRAY_A);
	
	return $ifnogo[0]['nogo'];
}

function getbuybutt_callback()
{	
	if(!empty($_GET))
		$data = $_GET;
	elseif(!empty($_POST))
		$data = $_POST;
		
	$ids = trim($data['id'], ',');
	$ids = explode(',', $ids);
	
	$ret = array();
	
	if(!isset($_SESSION['countryCode']))
	{
		$userIpData = getCountryByIP(); 
		$countryCode = $userIpData['countryCode'];
		$_SESSION['countryCode'] = $countryCode;
	}
	else
	{
		$countryCode = $_SESSION['countryCode'];
	}
	
	$nogo = checkIfNoGoZone($countryCode);
	
	foreach($ids as $id)
	{
		$str = '';
		$price = get_field('price', $id);
		$price_sales = trim(get_field('price_sales', $id));
		if(empty($price_sales))
		{
			$price2go = $price;
			$str_price = $price;
		}
		elseif(!empty($price_sales) && !empty($price))
		{
			$price2go = $price_sales;
			$str_price = '<span style="text-decoration:line-through">'.$price.' $</span>&nbsp;&nbsp;&nbsp;'.$price_sales;
		}
		else
		{
			$str_price = '';
			$price2go = '';
		}
		
		$item_number = get_field('item_number', $id);
		$ms_id = get_field('ms_id', $id);
		$out_of_stock = get_field('out_of_stock', $id);
				
		if($price != '')
		{
			$str .= '<div class="assh1">'.$str_price.' $</div>';
		} 
					 
		if(isset($out_of_stock[0]) && $out_of_stock[0] == 'Yes' && isset($countryCode) && in_array($countryCode, getAmazonctrs())) 
		{
			$amazon_link = get_field('amazon_link', $id);
			$buyamazon = get_option('buyamazon', $id);						
			$str .= '<div class="assh5">To deliver the desired tools as soon as possible, we opened a store on Amazon. This way We Save Your Time. Just click the orange button to check the price</div>';
			$str .= '<div class="assh6"><a id="ambtn" href="'.$amazon_link.'" target="_blank">'.$buyamazon.'</a></div>';
							
		} 
		elseif($nogo)
		{
			$str .= '<div class="assh5" style="font-size:16px; font-weight:bold;">Temporary Unavailable</div>';
		}
		else
		{ 
			$str .= '<div class="assh2">'.do_shortcode('[wp_cart_button name="'.get_the_title($id).'" price="'.$price2go.'" item_number="'.$item_number.'" ms_id="'.$ms_id.'"]').'</div>';
		}
		
		$ret[$id] = $str;
	}
	
	echo json_encode($ret);
	wp_die();
}

function findDefaultShipper($shippersList)
{
	$shippersPseudo = array_column($shippersList, 'pseudo');
	$shippersDefaults = array_column($shippersList, 'isdefault');
	$shippersDefaults = array_combine($shippersPseudo, $shippersDefaults);
	$shippersDefault = array_search(1, $shippersDefaults);
	
	return $shippersDefault;
}

function getshipmentdrop_callback()
{
	global $wpdb;

	$shippersList = $wpdb->get_results('SELECT * FROM shippers WHERE active=1 ORDER BY level ASC', ARRAY_A);
	$shippersPseudo = array_column($shippersList, 'pseudo');
	$shippersList = array_combine($shippersPseudo, $shippersList);
	
	$cart = $_SESSION['simpleCart'];
	
	$pricesList = array_column($cart, 'price');
	$quantityList = array_column($cart, 'quantity');
	
	$sumPrice = calculateSumPrice($pricesList, $quantityList);

	if(!empty($_GET))
		$data = $_GET;
	elseif(!empty($_POST))
		$data = $_POST;
	
	$id = $data['id'];
		
	$isFreeShT = get_field('free_shipping', $id);
	if(isset($isFreeShT[0]) && $isFreeShT[0] == 'Yes')
		$isFreeSh = true;
	else
		$isFreeSh = false;
	
	$weight = trim(get_field('weight', $id));
	$shipping_default = get_field('shipping_default', $id);
	$item_number = get_field('item_number', $id);
				
	if(!isset($_SESSION['countryCode']))
	{
		$userIpData = getCountryByIP(); 
		$countryCode = $userIpData['countryCode'];
		$_SESSION['countryCode'] = $countryCode;
	}
	else
	{
		$countryCode = $_SESSION['countryCode'];
	}
	
	$result = $wpdb->get_results('SELECT region FROM countries WHERE iso2="'.$countryCode.'"', ARRAY_A);
	$region_list = array('zone1','zone2','zone3','zone4','zone5','zone6','zone7','zone8','zone9','other');
	if(isset($result[0]['region']) && !empty($result[0]['region']))
	{
		$region = $result[0]['region'];
	
		foreach($shippersList as $key => $value)
		{
			$shippersList[$key]['hidden'] = false;
			
			if(!empty($value['restrict_cntrs']))
			{
				$restrict_cntrs = explode(',',$value['restrict_cntrs']);
				if(!in_array($countryCode, $restrict_cntrs))
					$shippersList[$key]['hidden'] = true;	
			}
		
			if($weight != ''  && in_array($region, $region_list))
			{
				$shipElPrice = $wpdb->get_results('SELECT max(z1.'.$region.') AS p FROM '.$value['prices_table'].' AS z1 WHERE z1.weight <= '.$weight, ARRAY_A);
				$shippersList[$key]['shipprice'] = $shipElPrice[0]['p'];
			}
			else
			{
				$shippersList[$key]['shipprice'] = $shipping_default;
			}
		}

	}
	else
	{
		$region = '';
	}
				
	$ret = '<div class="assh3">Ship to: 
				<select name="countries" id="countries" style="width:150px;">'.		
						generateCountryDropDown($countryCode).'</select>
			</div>';

	if($region != '' && !$isFreeSh) 
	{		
		$speed = $wpdb->get_results('SELECT '.implode(',',$shippersPseudo).' FROM speed WHERE zone="'.$region.'"', ARRAY_A);
		
		if($countryCode == 'US')
			$speed[0]['slow'] = '20-31 days';
		
		$selected = '';
		
		$ret .= '<div class="assh4">
					<span class="byw">&#160;&#160;by&#160;&#160;</span>
						<select name="speedB" id="speedB">';
						
						foreach($shippersList as $key => $value)
						{
							if($value['hidden'])
								continue;
							
							if(isset($_SESSION['ship_mode']) && ($_SESSION['ship_mode'] == $value['pseudo']))
								$selected = 'selected';
							elseif(!isset($_SESSION['ship_mode']) && $value['isdefault'])
								$selected = 'selected';
							else
								$selected = '';
							
							if(($value['freeonlimit'] != 0) && ($sumPrice > $value['freeonlimit']))
								$value['shipprice'] = 0;
			
							if(!empty($value['freeforzone']))
							{
								$freeforzones = explode(',', $value['freeforzone']);
								if(in_array($region, $freeforzones))
									$value['shipprice'] = 0;
							}
							
							
							if(!empty($value['altershiptime']))
							{
								$altershiptime = json_decode(stripslashes($value['altershiptime']), true);
								if(isset($altershiptime[$countryCode]))
									$speed[0][$value['pseudo']] = $altershiptime[$countryCode];
							}
								
							$ret .= '<option value="'.$value['pseudo'].'" '.$selected.'>'.$value['name'].' ('.$speed[0][$value['pseudo']].') | '.printShPrice($value['shipprice']).'</option>';
						}
		$ret .= '</select>
			</div>';
		
	} 
	else 
	{ 
		$ret .= printShPrice($shipping_default);
	}
	
	echo $ret;
	
	wp_die();
}

function curlMS($url, $data, $method)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, "login:pwd");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	$result = curl_exec($ch);
	$result = json_decode($result, true);
	curl_close($ch);
	
	return $result;
}
	
	?>