<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 *
 * @package ThinkUpThemes
 */

get_header(); 

$post = print_r($_POST, true);

if(isset($_POST))
{
	$ppresponce = json_encode($_POST);
	$invoiceT = $_POST['invoice'];
	$email = $_POST['payer_email'];
	$invoiceT = explode('-', $invoiceT);
	$invoice = $invoiceT[1];
	$payment_status = addslashes($_POST['payment_status']);
	
	if(isset($_POST['first_name'])) {$first_name = addslashes($_POST['first_name']);} else {$first_name='';}
	if(isset($_POST['last_name'])) {$last_name = addslashes($_POST['last_name']);} else {$last_name = '';}
	if(isset($_POST['address_country'])) {$address_country = addslashes($_POST['address_country']);} else {$address_country = '';}
	if(isset($_POST['address_city'])) {$address_city = addslashes(nl2br($_POST['address_city']));} else {$address_city = '';}
	if(isset($_POST['address_street'])) {$address_street = addslashes($_POST['address_street']);} else {$address_street = '';}
	if(isset($_POST['address_state'])) {$address_state = addslashes($_POST['address_state']);} else {$address_state = '';}
	if(isset($_POST['payment_gross'])) {$payment_gross  = addslashes($_POST['payment_gross']);} else {$payment_gross = '';}
	if(isset($_POST['address_zip'])) {$address_zip  = addslashes($_POST['address_zip']);} else {$address_zip = '';}
	if(isset($_POST['payer_email'])) {$payer_email  = addslashes($_POST['payer_email']);} else {$payer_email = '';}
	
	update_post_meta($invoice, 'wpsc_status', $payment_status);
	update_post_meta($invoice, 'wpsc_ppresponce', $ppresponce);
	
	update_post_meta($invoice, 'wpsc_first_name', $first_name);
	update_post_meta($invoice, 'wpsc_last_name', $last_name);
	update_post_meta($invoice, 'wpsc_country', $address_country);
	update_post_meta($invoice, 'wpsc_city', $address_city);
	update_post_meta($invoice, 'wpsc_address1', $address_street);
	update_post_meta($invoice, 'wpsc_state',$address_state);
	update_post_meta($invoice, 'wpsc_total_amount', $payment_gross);
	update_post_meta($invoice, 'wpsc_zip', $address_zip);
	update_post_meta($invoice, 'wpsc_email_address', $payer_email);
	
	
	
	if(in_array($payment_status,array('Completed', 'Pending', 'Processed')))
	{
		/***************MS sync********************/
		$buyerName = trim($first_name.' '.$last_name);
		if(isset($_POST['contact_phone']))
			$buyerPhone = $_POST['contact_phone'];
		else
			$buyerPhone = '';
		
		$shAsPr = 'fe0e16c9-bd11-11e9-9107-5048001e0676';
		
		global $wpdb;
		$wpspsc_items_ordered = $wpdb->get_results('SELECT meta_value FROM bc_postmeta WHERE meta_key="wpspsc_items_ordered" AND post_id='.intval($invoice) ,ARRAY_A);
		$wpspsc_items_ordered = json_decode($wpspsc_items_ordered[0]['meta_value'],true);
		$goodsT = array_column($wpspsc_items_ordered, 'item_number');
		$goods_qu = array_column($wpspsc_items_ordered, 'quantity');
		$goods_pr = array_column($wpspsc_items_ordered, 'price');
		
		$goodsMsId = $wpdb->get_results('SELECT bc2.post_id, bc2.meta_value AS item_number, bc1.meta_value AS ms_id FROM bc_postmeta AS bc1 JOIN bc_postmeta AS bc2 ON bc1.post_id=bc2.post_id WHERE bc2.meta_key="item_number" AND bc2.meta_value IN ("'.implode('","',$goodsT).'") AND bc1.meta_key="ms_id"', ARRAY_A);

		$goodsMsIdKeys = array_column($goodsMsId, 'item_number');
		$goodsMsId = array_combine($goodsMsIdKeys, $goodsMsId);
		
		$gtC = count($goodsT);
		$goods = array();
		for($i = 0; $i < $gtC; $i++)
		{
			$boxT = get_field('box', $goodsMsId[$goodsT[$i]]['post_id']);
			$goods[$goodsT[$i]] = array('quantity' => $goods_qu[$i], 'price' => $goods_pr[$i], 'ms_id' => $goodsMsId[$goodsT[$i]]['ms_id'], 'box' => $boxT);
		}
		
		$wpsc_total_amount = $wpdb->get_results('SELECT meta_value FROM bc_postmeta WHERE meta_key="wpsc_total_amount" AND post_id='.intval($invoice) ,ARRAY_A);
		$price = $wpsc_total_amount[0]['meta_value']*100;
		$total = $price;
		
		$wpsc_shipping_amount = $wpdb->get_results('SELECT meta_value FROM bc_postmeta WHERE meta_key="wpsc_shipping_amount" AND post_id='.intval($invoice) ,ARRAY_A);
		$shipping = $wpsc_shipping_amount[0]['meta_value']*100;
		
		
		$formula = (($price+$shipping)*0.019)+0.3+($price+$shipping-(($price+$shipping)*0.019+0.3))*0.04+($price+$shipping)*0.05 + ($price+$shipping-((($price+$shipping)*0.019)+0.3+($price+$shipping -(($price+$shipping)*0.019+0.3))*0.04+($price+$shipping)*0.05))*0.04;
		
		$dd = date('Y-m-d H:i:s');
		
		$input['organization'] = array(
				  'meta' => array(
							'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/organization/91770759-8ec1-4cab-acb7-841344b8d4d4',
						    'type' => 'organization',
						    'mediaType' => 'application/json'
							)
				  );
				  
		$input['agent'] = array(
					'meta' => array(
						'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/counterparty/9356c2d3-ddda-11e8-9ff4-3150000a0863',
						'type' => 'counterparty',
						'mediaType' => 'application/json'
						)
					);
					
		$input['project'] = array('meta' =>array(
										'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/project/4f6bd091-5608-11e9-912f-f3d400119935',
										'metadataHref' => 'https://online.moysklad.ru/api/remap/1.1/entity/project/metadata',
										'type' => 'project',
										'mediaType' => 'application/json'
									));
									
		$input['store'] = array('meta' =>array(
										'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/store/7f7876ea-44e9-458b-89ea-8191580bcd69',
										'metadataHref' => 'https://online.moysklad.ru/api/remap/1.1/entity/store/metadata',
										'type' => 'store',
										'mediaType' => 'application/json'
									));
									
		$input['state'] = array("meta" => array(
								"href" => "https://online.moysklad.ru/api/remap/1.1/entity/customerorder/metadata/states/5dd691e8-4471-11e8-9ff4-31500003a653",
								"type" => "state"));
								
		$input['rate'] = array('currency' =>array('meta' =>array(
										'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/currency/0c54db89-bd44-11e2-acb6-001b21d91495', 
										'metadataHref' => 'https://online.moysklad.ru/api/remap/1.1/entity/currency/metadata',
										'type' => 'currency',
										'mediaType' => 'application/json'
									)));
									
		$input['positions'] = array();
		
		foreach($goods as $good)
		{
			$input['positions'][] = array(
								  'quantity' => $good['quantity'],
								  'price' => $good['price']*100,
								  'discount' => 0,
								  'vat' => 18,
								  'assortment' => array(
									'meta' => array(
									  'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/product/'.$good['ms_id'],
									  'type' => 'product',
									  'mediaType' => 'application/json'
									  )
									),
								  'reserve' => 0
									);
									
			/****Check if box needed****/
			if(isset($good['box']) && !empty($good['box']) && (trim($good['box']) != '-'))
			{
				$boxData = array('quantity' => 1,
								'price' => 0,
								'discount' => 0,
								'vat' => 0,
								'assortment' =>array('meta' =>array(
									'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/product/'.$good['box'],
									'type' => 'product',
									'mediaType' => 'application/json',
								)));
								
				$input['positions'][] = $boxData;
			}
			/***************/
		}
		
		//Add shipping as service
		$shippingAsGood = array('quantity' => 1,
							'price' => $shipping,
							'discount' => 0,
							'vat' => 0,
							'assortment' =>array('meta' =>array(
								'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/service/'.$shAsPr,
								'type' => 'service',
								'mediaType' => 'application/json',
							)));

		array_push($input['positions'], $shippingAsGood);

		/******Order*************/

		$orderData = $input;

		$orderData['name'] = '';
		$orderData['description'] = 'Test order by OB';
		$orderData['code'] = '';
		$orderData['moment'] = $dd;
		$orderData['applicable'] = false;
		$orderData['vatEnabled'] = false;
		$orderData['attributes'] = array(
			0 => array('id' => '2bf3a64a-cdd9-11e2-0ae9-001b21d91495', 'value' => $buyerName),
			1 => array('id' => '55a17ccb-ce93-11e2-fa42-7054d21a8d1e', 'value' => $buyerPhone),
			2 => array('id' => '4a2ba9c7-d802-11e2-cf22-7054d21a8d1e', 'value' => array('name' => 'PayPal'))
			);
			
		$orderUrl = 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder';
	
		$orderResult = curlMS($orderUrl, $orderData, 'POST');
			//print_r($orderData);
			//print_r($orderResult);

		$customerOrder = array('meta' =>array(
									'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder/'.$orderResult['id'],
									'type' => 'customerorder',
									'mediaType' => 'application/json'));

		echo 'Order created. Name'.$orderResult['name'].' MS id: '.$orderResult['id'].'<br>';

		/****Demand****/

		$demandData = $input;
		$demandData['name'] = '';

		$demandData['code'] = '';
		$demandData['moment'] = $dd;
		$demandData['applicable'] = true;
		$demandData['vatEnabled'] = true;
		$demandData['vatIncluded'] = true;

		$demandData['customerOrder'] = $customerOrder;

		$demandUrl = 'https://online.moysklad.ru/api/remap/1.1/entity/demand';

		$demandResult = curlMS($demandUrl, $demandData, 'POST');

		echo 'Demand created. Name'.$demandResult['name'].' MS id: '.$demandResult['id'].'<br>';

		/****Payment in product****/

		$paymentInProductData = $input;

		  $paymentInProductData['name'] = '';
		  $paymentInProductData['operations'] = array(
												array('meta' => array(
															'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder/' .$orderResult['id'],
															'metadataHref' => 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder/metadata',
															'type' => 'customerorder',
															'mediaType' => 'application/json',
															),
												'linkedSum' => $total  )
											);
		  $paymentInProductData['sum'] = $total;

		$paymentInProductUrl = 'https://online.moysklad.ru/api/remap/1.1/entity/paymentin';
		$paymentInProductResult = curlMS($paymentInProductUrl, $paymentInProductData, 'POST');


		echo 'Payment in for product is created. Name'.$paymentInProductResult['name'].' MS id: '.$paymentInProductResult['id'].'<br>';
		
		/****Payment not binded out with magic***/

		$paymentOutMagicData = $input;

		$paymentOutMagicData['name'] = '';
		 
		$paymentOutMagicData ['expenseItem'] = array('meta' => array(
									'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/expenseitem/2b15e993-7e9c-11e8-9ff4-3150000398cb',                                         
									'metadataHref' => 'https://online.moysklad.ru/api/remap/1.1/entity/expenseitem/metadata',
									'type' => 'expenseitem',
									'mediaType' => 'application/json'));
		$paymentOutMagicData['applicable'] = false;
		$paymentOutMagicData['sum'] = $formula;

								
		$paymentOutMagicUrl = 'https://online.moysklad.ru/api/remap/1.1/entity/paymentout';
		$paymentOutMagicResult = curlMS($paymentOutMagicUrl, $paymentOutMagicData, 'POST');

		echo 'Payment out with magic is created. Name'.$paymentOutMagicResult['name'].' MS id: '.$paymentOutMagicResult['id'].'<br>';


		/****Empty payment out for manual data add*****/

		$paymentOutEmptyData = $input;

		$description = 'Заказ №'.$invoice.' От:'.$buyerName .' Отгрузка: '.$demandResult['name'].' Вход.за.тов:'.$paymentInProductResult['name'].' Выход.форм: '.$paymentOutMagicResult['name'];

		$paymentOutEmptyData['name'] = '';
		 
		$paymentOutEmptyData ['expenseItem'] = array('meta' => array(
									'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/expenseitem/113ab839-68b9-11e8-9ff4-3150000865ab',                                         
									'metadataHref' => 'https://online.moysklad.ru/api/remap/1.1/entity/expenseitem/metadata',
									'type' => 'expenseitem',
									'mediaType' => 'application/json'));
		$paymentOutEmptyData['applicable'] = false;
		$paymentOutEmptyData['sum'] = $formula;

		$paymentOutEmptyData['description'] = $description; 
		$paymentOutEmptyData['paymentPurpose'] = $description;

								
		$paymentOutEmptyUrl = 'https://online.moysklad.ru/api/remap/1.1/entity/paymentout';
		$paymentOutEmptyResult = curlMS($paymentOutEmptyUrl, $paymentOutEmptyData, 'POST');

		echo 'Payment out empty is created. Name'.$paymentOutEmptyResult['name'].' MS id: '.$paymentOutEmptyResult['id'].'<br>';

		/****Update order*****/

		$orderUpdateUrl = 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder/'.$orderResult['id'];
		$orderUpdate['description'] = $description;
		$orderUpdateResult = curlMS($orderUpdateUrl, $orderUpdate, 'PUT');

		/****Update demand*****/

		$demandUpdateUrl = 'https://online.moysklad.ru/api/remap/1.1/entity/demand/'.$demandResult['id'];
		$demandUpdate['description'] = $description;
		$demandUpdateResult = curlMS($demandUpdateUrl, $demandUpdate, 'PUT');

		/****Update payment in product*****/

		$payInPrUpdateUrl = 'https://online.moysklad.ru/api/remap/1.1/entity/paymentin/'.$paymentInProductResult['id'];
		$payInPrUpdate['description'] = $description;
		$payInPrUpdateResult = curlMS($payInPrUpdateUrl, $payInPrUpdate, 'PUT');

		/****Update out magic*****/

		$payOutMUpdateUrl = 'https://online.moysklad.ru/api/remap/1.1/entity/paymentout'.$paymentOutMagicResult['id'];
		$payOutMUpdate['description'] = $description;
		$payOutMUpdateResult = curlMS($payOutMUpdateUrl, $payOutMUpdate, 'PUT');
		
		/*******************************************/
		
		$sh_operator = '';
		$coupon = get_post_meta($invoice, 'coupon', true);
		$myobj2 = get_option('wpspsc_coupons_collection');
		$myobj2->markasused_by_code($coupon);
		
		$mailchimp = htmlspecialchars_decode(get_option('substxtbox4'));
		
		$wpsc_sh_option = get_post_meta($invoice, 'wpsc_sh_option', true);
		
		if($wpsc_sh_option == 'ex')
		{
			$sh_operator = get_option('shipser1');
		}
		elseif($wpsc_sh_option == 'cheap')
		{
			$sh_operator = get_option('shipser2');
		}
		
		global $wpdb;
		
		$regionT = $wpdb->get_results('SELECT region FROM countries WHERE country="'.$address_country.'"', ARRAY_A);
		
		if(isset($regionT[0]['region']) && !empty($regionT[0]['region']))
			$region = $regionT[0]['region'];
		else
			$region = 'other';
		
		/*$speedT = $wpdb->get_results('SELECT slow, fast FROM speed WHERE zone="'.$region.'"', ARRAY_A);
		
		if($wpsc_sh_option == 'ex')
		{
			$speed_sh = $speedT[0]['fast'];
		}
		elseif($wpsc_sh_option == 'cheap')
		{
			$speed_sh = $speedT[0]['slow'];
		}*/
		
		$speed_sh = '';
		
		$num_items = $_POST['num_cart_items'];
		
		$nip = $num_items+1;
		
		$items = '<table cellpadding="2" cellspacing="2">';
		$items = '<tr><td width="50%"><b>Item name</b></td><td width="25%"><b>Quantity</b></td><td width="25%"><b>Price</b></td></tr>';
		$items = '<tr><td colspan="3"><hr style="height:2px; color: #666; background-color: #666;"></td></tr>';

		for($i = 1; $i < $nip; $i++)
		{
			$items .= '<tr><td>'.$_POST['item_name'.$i].'</td><td>'.$_POST['quantity'.$i].'</td><td>'.$_POST['mc_gross_'.$i].'</td></tr>';
		}
		
		$items .= '<tr><td colspan="3"><hr style="height:2px; color: #666; background-color: #666;"></td></tr>';
		$items .= '<tr><td colspan="2" align="right">Shipping ('.$sh_operator.', '.$speed_sh.'):</td><td>15</td></tr>';
		$items .= '<tr><td colspan="2" align="right"><b>Total amount:</b></td><td>'.$_POST['payment_gross'].'</td></tr>';
		$items .= '</table>';
		
		$subject = 'ORDER #'.$invoice.' at Beavercrafttools.com';
		
		$message = '<html>
						<head>
						<style type="text/css">
								body, td, span, p, th { font-size: 12px; font-family: Verdana, Arial, Helvetica, sans-serif; }
								h3 {font-size: 18px; font-weight:bold; margin:0}
								h4 {font-size: 15px; font-weight:bold; margin:0}
							table.html-email {margin:10px auto;background:#fcfcfc;border:none;}
							.html-email tr{border-bottom : 1px solid #eee; margin: 10px 0;}
							.html-email td a {color: #6C8F20;font-family: Tahoma,sen-serif;	font-size: 13px;}
							.html-email td a:hover {color: #4B611D}
							span.grey {color:#666;}
							span.date {color:#666; }
							a.default:link, a.default:hover, a.default:visited {color:#666;line-height:25px;background: #f2f2f2;margin: 10px ;padding: 3px 8px 1px 8px;border: solid #CAC9C9 1px;border-radius: 4px;-webkit-border-radius: 4px;-moz-border-radius: 4px;text-shadow: 1px 1px 1px #f2f2f2;font-size: 12px;background-position: 0px 0px;display: inline-block;text-decoration: none;}
							a.default:hover {color:#888;background: #f8f8f8;}
							.cart-summary{ }
							.html-email th { background: #ccc;margin: 0px;padding: 5px 10px;}
							.sectiontableentry2, .html-email th, .cart-summary th{ background: #ccc;margin: 0px;padding: 5px 10px;}
							.sectiontableentry1, .html-email td, .cart-summary td {background: #fcfcfc;margin: 0px;padding: 3px 10px;}
							.line-through{text-decoration:line-through}
						</style>

						</head>

						<body style="background: #fcfcfc;word-wrap: break-word;">
						<div style="background-color: #fcfcfc;" width="100%">
							<table style="margin: auto;" cellpadding="0" cellspacing="0" width="600" >
								<tr>
									<td>						
										<table width="100%" border="0" cellpadding="0" cellspacing="0" class="html-email">
											<tr>
												<td width="40%" align="top">
													<img src="https://beavercrafttools.com/wp-content/uploads/2018/06/Logo_200x40_trans_bold.jpg" />
												</td>
												<td width="60%">
													Beavercrafttools.com<br>High-quality woodcarving tools.
												</td>
											</tr>
											<tr>
												<td colspan="2">
													Succesful payment for Order #'.$invoice.' .
													Our delivery department will process it as fast as possible.
												</td>
											</tr>
											<tr>
												<td colspan="2">
													Your order:
												</td>
											</tr>
											<tr>
												<td colspan="2">
													'.$items.'
												</td>
											</tr>
											<tr>
												<td colspan="2">
													'.$mailchimp.'
												</td>
											</tr>
										</table>
										<table class="html-email" width="100%" cellspacing="0" cellpadding="0" border="0">

											<tr align="left" class="sectiontableheader">
												<th width="100%" align="left"><h4>
													
												</h4></th>
											</tr>
										</table>
										
									</td>
								</tr>
								<tr>
									<td style="text-align:center"><br/>
										Beavercrafttools.com - high-quality woodcarving tools
									</td>
								</tr>
							</table>
						</div>
						</body>
						</html>';
					
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
			$headers .= 'From: Beavercrafttools <info@beavercrafttools.com>' . "\r\n";
			$headers .= 'Cc: info@beavercrafttools.com';

			wp_mail($email, $subject, $message, $headers);
	}
	
	
}

get_footer(); ?>