<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 *
 * @package ThinkUpThemes
 */

get_header(); 

if(isset($_SESSION['simpleCart']))
{
	global $wpdb;
	$shippersList = $wpdb->get_results('SELECT * FROM shippers WHERE active=1 ORDER BY level ASC', ARRAY_A);
	$shippersPseudo = array_column($shippersList, 'pseudo');
	$shippersList = array_combine($shippersPseudo, $shippersList);
	
	if(!isset($_SESSION['order_id']))
	{
		$order = array(
		'post_type' => 'wpsc_cart_orders',
		'post_status' => 'publish',
		'post_title' => 'ORDER AT '.date('d-m-Y H:i:s')
		);
		$post_id = wp_insert_post( $order );
		
		$_SESSION['order_id'] = $post_id;
	}
	else
	{
		$post_id = $_SESSION['order_id'];
	}
	
	$ship_priceT = calculateShippingPrice();
	
	$shippersDefault = findDefaultShipper($shippersList);

	if(isset($_SESSION['ship_mode']) && isset($shippersList[$_SESSION['ship_mode']]))
	{
		$shipperSelected = $shippersList[$_SESSION['ship_mode']];
	}
	else
	{
		$shipperSelected = $shippersList[$shippersDefault];
	}
	
	$ship_mode = $shipperSelected['pseudo'];
	$shipResult = $ship_priceT[$shipperSelected['pseudo']];
	$ship_table = $shipperSelected['prices_table'];
	
	/****Obtain client IP******/
	$client_ip = @$_SERVER['HTTP_CLIENT_IP'];
    $forward_ip = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote_ip = @$_SERVER['REMOTE_ADDR'];
    if (filter_var($client_ip, FILTER_VALIDATE_IP)) {
      $res_ip = $client_ip;
    } elseif (filter_var($forward_ip, FILTER_VALIDATE_IP)) {
      $res_ip = $forward_ip;
    } else {
      $res_ip = $remote_ip;
    }
	/***************************/
	
	$res_amount = 0;
	$ship_price = 0;
	
	/**********Get region****************/
	if(!isset($_SESSION['countryCode']))
	{
		$userIpData = getCountryByIP(); 
		$countryCode = $userIpData['countryCode'];
		$_SESSION['countryCode'] = $countryCode;
	}
	else
	{
		$countryCode = $_SESSION['countryCode'];
	}
	
	if(isset($_SESSION['wpspsc_applied_coupon_code']))
		$coupon = $_SESSION['wpspsc_applied_coupon_code'];
	else
		$coupon = '';
	
	$countries = $wpdb->get_results('SELECT country, iso2 FROM countries', ARRAY_A);
	
	$result = $wpdb->get_results('SELECT region FROM countries WHERE iso2="'.$countryCode.'"', ARRAY_A);
	if(isset($result[0]['region']) && !empty($result[0]['region']))
		$region = $result[0]['region'];
	else
		$region = '';
	
	$region_list = array('zone1','zone2','zone3','zone4','zone5','zone6','zone7','zone8','zone9','other');
	/****************************************/
	
	$scCount = count($_SESSION['simpleCart']);
	$scKeys = range(1, $scCount);

	$simpleCart = array_combine($scKeys, $_SESSION['simpleCart']);
	?>
	<form method="post" id="paypalform" action="https://shop.westernbid.info">
	<input type="hidden" name="charset" value="utf-8">
	<input type="hidden" name="wb_login" value="woodcarvingtools">
	<input type="hidden" name="invoice" value="<?php echo $post_id; ?>">
	<table>
		<tr><td>ORDER #<?php echo $post_id; ?>: </td><td></td></tr>
		<tr><td>E-mail: </td><td><input type="text" name="email" style="width:100%"></td></tr>
		<tr><td>First name: </td><td><input type="text" name="first_name" style="width:100%"></td></tr>
		<tr><td>Last name </td><td><input type="text" name="last_name" style="width:100%"></td></tr>
		<tr><td>Address line 1: </td><td><input type="text" name="address1" style="width:100%"></td></tr>
		<tr><td>Address line 2: </td><td><input type="text" name="address2" style="width:100%"></td></tr>
		<tr><td>Country: </td>
			<td>
				<select name="country" style="width:100%">
				<?php 
					foreach($countries as $cntr)
					{
						if($cntr['iso2'] == strtoupper($countryCode))
							$selected = 'selected';
						else
							$selected = '';
						echo '<option value="'.$cntr['country'].'" '.$selected.'>'.$cntr['country'].'</option>';
					}
				?>
				</select>
			</td>
		</tr>
		<tr><td>Phone: </td><td><input type="text" name="phone" style="width:100%"></td></tr>
		<tr><td>City: </td><td><input type="text" name="city" style="width:100%"></td></tr>
		<tr><td>State (if USA): </td><td><input type="text" name="state" style="width:100%"></td></tr>
		<tr><td>ZIP code: </td><td><input type="text" name="zip" style="width:100%"></td></tr>
	<input type="hidden" name="discount_amount" value="0">
	<input type="hidden" name="coupon" value="<?php echo $coupon; ?>">
	<input type="hidden" name="currency_code" value="USD">
	<?php
	$total_weight = 0;
	$cfs = 0;
	$numOrd = 1;
	foreach($simpleCart as $key => $value)
	{
		if(!isset($value['name']))
			continue;
		$res_amount += ($value['quantity']*$value['price']);
		
		$it2pid = $wpdb->get_results('SELECT pm.post_id FROM bc_postmeta AS pm WHERE pm.meta_key="item_number" AND pm.meta_value="'.$value['item_number'].'"', ARRAY_A);
		$product_id = $it2pid[0]['post_id'];
		$product_weight = get_field('weight', $product_id);
		$product_isfree = get_field('free_shipping', $product_id);
		if(isset($product_isfree[0]))
			$product_isfree = $product_isfree[0];
		else
			$product_isfree = '';
		$product_shipping_default = get_field('shipping_default', $product_id);
		
		$product_weight = $product_weight*$value['quantity'];
		
		if($region != '' && in_array($region, $region_list))
		{
			$shipprice_ind = $wpdb->get_results('SELECT max(z1.'.$region.') AS prc FROM '.$ship_table.' AS z1 WHERE z1.weight <= '.$product_weight, ARRAY_A);
			$shipprice_ind = $shipprice_ind[0]['prc'];
		}
		else
		{
			$shipprice_ind = $product_shipping_default;
		}
		
		if(ucfirst($product_isfree) == 'Yes')
			$shipprice_ind = 0;
		
		$ship_price += $shipprice_ind;
		
		$total_weight += $product_weight;
		
		$shipser = $shipperSelected['name'];
		
		if(!empty($shipperSelected['altershiptime']))
		{
			$altershiptime = json_decode(stripslashes($shipperSelected['altershiptime']), true);
			if(isset($altershiptime[$countryCode]))
				$speedf = $altershiptime[$countryCode];
		}
	
		if(!isset($speedf))
		{
			$speed = $wpdb->get_results('SELECT '.implode(',', $shippersPseudo).' FROM speed WHERE zone="'.$region.'"', ARRAY_A);
			$speedf = $speed[0][$shipperSelected['pseudo']];
		}
		
		if(!$cfs)
			$stupidShip = $shipResult;
		else
			$stupidShip = 0;
		
		$cfs++;
	?>
	<input type="hidden" name="shipping_<?php echo $numOrd; ?>" value="<?php echo $stupidShip; //echo $shipprice_ind; ?>">
	<input type="hidden" name="item_name_<?php echo $numOrd; ?>" value="<?php echo $value['name']; ?>">
	<input type="hidden" name="item_number_<?php echo $numOrd; ?>" value="<?php echo $value['item_number']; ?>">
	<input type="hidden" name="url_<?php echo $numOrd; ?>" value="<?php echo get_the_permalink($product_id); ?>">
	<input type="hidden" name="description_<?php echo $numOrd; ?>" value="<?php echo $value['name']; ?>">
	<input type="hidden" name="amount_<?php echo $numOrd; ?>" value="<?php echo $value['price']; ?>">
	<input type="hidden" name="quantity_<?php echo $numOrd; ?>" value="<?php echo $value['quantity']; ?>">
	<?php 
	$numOrd++;
	} ?>
	<input type="hidden" name="item_name" value="<?php echo $post_id; ?>">
	<tr><td>Sub Total amount, USD: </td><td><input type="text" name="amount_show" value="$ <?php echo $res_amount; ?>" readonly style="width:100%"></td></tr>
	<tr><td>Shipping (<?php echo $speedf;?>, <?php echo $shipser; ?>), USD: </td><td><input type="text" name="amount_show" value="$ <?php echo $shipResult; ?>" readonly style="width:100%"></td></tr>
	<tr><td>Total amount, USD: </td><td><input type="text" name="amount_show" value="$ <?php echo ($res_amount+$shipResult); ?>" readonly style="width:100%"></td></tr>
	<input type="hidden" name="shipping_amount" value="<?php echo $shipResult; ?>">
	<input type="hidden" name="sub_amount" value="<?php echo $res_amount; ?>">
	<input type="hidden" name="sh_option" value="<?php echo $ship_mode; ?>">
	<input type="hidden" name="ip" value="<?php echo $res_ip; ?>">
	<input type="hidden" name="amount" value="<?php echo ($res_amount+$shipResult); ?>">
	<input type="hidden" name="wb_hash" value="<?php echo md5('woodcarvingtools'.'GU"Sn9+'.($res_amount+$shipResult).$post_id); ?>">
	<input type="hidden" name="return" value="<?php echo site_url(); ?>/payment-result/">
	<input type="hidden" name="cancel_return" value="<?php echo site_url(); ?>/cancel-return/">
	<input type="hidden" name="notify_url" value="<?php echo site_url(); ?>/notify/">
	<!--<tr><td colspan="2"><input type="button" id="smbt" onclick="catchdata();"value="Submit"></td></tr>-->
	<tr><td colspan="2"><input type="submit" id="smbt" value="Submit"></td></tr>
	</table>
	</form>
			

	<?php 
}
get_footer(); ?>