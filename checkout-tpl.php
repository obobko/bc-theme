<?php
/**
 * Template Name: Checkout
 */
if(!isset($_SESSION['simpleCart']))
{
	wp_redirect(site_url());
	die();
}

get_header(); ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php /* Add comments */  consulting_thinkup_input_allowcomments(); ?>

			<?php endwhile; ?>

<?php get_footer(); ?>