<?php
/* Template Name: Gallery */

get_header(); 
?>

<style>
main {display:table;}
.cards {
    display: flex;
    flex-wrap: wrap;
    align-items: flex-start;
    flex-direction: row;
  }
  .cards div {
    margin: 10px;
    max-width: 23vw;
  }
  
  
.hovereffect {
width:100%;
height:100%;
float:left;
overflow:hidden;
position:relative;
text-align:center;
cursor:default;
}

.hovereffect .overlay {
width:93%;
height:100%;
position:absolute;
overflow:hidden;
top:-10px;
left:-10px;
opacity:0;
-webkit-transition:all .4s ease-in-out;
transition:all .4s ease-in-out
}

.hovereffect img {
display:block;
position:relative;
}

.hovereffect h2 {
text-transform:uppercase;
color:#fff;
text-align:center;
position:absolute;
bottom:-15px;
width:97%;
font-size:17px;
background:rgba(0,0,0,0.6);
-webkit-transform:translatey(-100px);
-ms-transform:translatey(-100px);
transform:translatey(-100px);
-webkit-transition:all .2s ease-in-out;
transition:all .2s ease-in-out;
padding-bottom:10px;
padding-top:10px;
}

.hovereffect:hover img {

}

.hovereffect:hover .overlay {
opacity:1;
filter:alpha(opacity=100);
}

.hovereffect:hover h2{
opacity:1;
font-size:14px !important;
font-weight: 400 !important;
filter:alpha(opacity=100);
-ms-transform:translatey(0);
-webkit-transform:translatey(0);
transform:translatey(0);
}
</style>
<main class="cards">
			<?php
			if(get_query_var('paged')) 
			{ 
				$paged = get_query_var('paged');
			}
			elseif(get_query_var('page')) 
			{
				$paged = get_query_var('page'); 
			}
			else
			{
				$paged = 1;
			}

			$the_query = new WP_Query( array('post_type' => 'gallery',  'posts_per_page' => 30, 'paged' => $paged) );
			if( $the_query->have_posts() ){ 
			while( $the_query->have_posts() ){ 
			  $the_query->the_post();
			$content = get_the_content();
			?>
			
			<?php
			$post_id = get_the_ID();
			
			$images = acf_photo_gallery('gallery', $post_id);		
			
			$title = get_the_title();
			
			$author = get_field('author', $post_id);
			$opys = get_field('opys', $post_id);
			
			if(!empty($author))
			{
				$title .= ' - '.$author;
			}
			
			$i = 0;

				
				?>
				<div>
					<div class="hovereffect">
					<?php 
					foreach($images as $image)
					{
						$full_image_url = $image['full_image_url']; //Full size image url
						$reqImgUrl = acf_photo_gallery_resize_image($full_image_url, 180, 180); 
						
						if(!$i)
						{
					?>				
						<a href="<?php echo $full_image_url; ?>" data-fancybox="gallery" data-caption="<b><?php echo $title; ?></b><br /><?php echo $opys; ?>">
							<img class="lazy lazy-hidden" src="//beavercrafttools.com/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif" data-lazy-type="image" data-src="<?php echo $reqImgUrl; ?>">
							<noscript><img src="<?php echo $reqImgUrl; ?>"></noscript>
							<div class="overlay">
									<h2><?php echo $title; ?></h2>
							</div>
						</a>
						
						<?php
						}
						else
						{
							?>
							<a href="<?php echo $full_image_url; ?>" data-fancybox="gallery" data-caption="<?php echo $title; ?>">
							</a>
							<?php
						}
						$i++;
					}
					?>
							
					</div>
				</div>
				
			<?php
		  }
		}		  ?>
			 </main>
			  <div class="paging">
						  </br>
					 <?php
					 
					
					 
					 $total_pages = $the_query->max_num_pages;
					if($total_pages > 1)
					{
						for($i = 1; $i <= $total_pages; $i++)
						{
							if($i == 1)
								$pTail = '';
							else
								$pTail = '?page='.$i;
							echo '<a class="page-numbers" href="'.site_url().'/gallery/'.$pTail.'">     '.$i.'</a> ';
						}
					}
					
					 wp_reset_query();
					
				?>
				</div>
				<div style="clear:both"></div>
<?php get_footer(); ?>