<?php
/**
 * The Single Post content template file.
 *
 * @package ThinkUpThemes
 */
?>		
<style type="text/css">

/*.zoomLens {
    width: 100px !important;
    height: 100px !important;*/
/*}*/

.element_13 {
	position: relative;
	width:93%; 
	margin:5px 0px 5px 0px;
	padding:2%;
	clear:both;
	overflow: hidden;
	/*border:1px solid #dedede;
	background:#f9f9f9;*/
}

.element_13 > div {
	display:table-cell;
}

.element_13 div.left-block_13 {
	padding-right:10px;
}

.element_13 div.left-block_13 .main-image-block_13 {
	clear:both;
	width:240px; 
}

.element_13 div.left-block_13 .main-image-block_13 img {
	margin:0px !important;
	padding:0px !important;
	width:240px !important; 
	height:auto;
}

.element_13 div.left-block_13 .thumbs-block {
	position:relative;
	margin-top:10px;
}

.element_13 div.left-block_13 .thumbs-block ul {
	width:240px; 
	height:auto;
	display:table;
	margin:0px;
	padding:0px;
	list-style:none;
}

.element_13 div.left-block_13 .thumbs-block ul li {
	margin:0px 3px -22px 2px;
	padding:0px;
	width:75px; 
	height:75px; 
	float:left;
}

.element_13 div.left-block_13 .thumbs-block ul li a {
	display:block;
	width:75px; 
	height:75px; 
}

.element_13 div.left-block_13 .thumbs-block ul li a img {
	/*margin:0px !important;
	padding:0px !important;
	width:75px; 
	height:75px; */
}

.element_13 div.right-block {
	vertical-align:top;
}

.element_13 div.right-block > div {
	width:100%;
	padding-bottom:10px;
	margin-top:10px;
			background:url('http://beavercraft.com.ua/wp-content/plugins/product-catalog/Front_end/../images/divider.line.png') center bottom repeat-x;
		
}

.element_13 div.right-block > div:last-child {
	background:none;
}

.element_13 div.right-block .title-block_13  {
	margin-top:-4px;
}

.element_13 div.right-block .title-block_13 h3 {
	margin:0px;
	padding:0px;
	font-weight:bold;
	font-size:18px !important;
	line-height:22px !important;
	color:#5e5e5e;
	margin-left:8px;
}

.description-block_13 em, .description-block_13 i, .description-block_13 p em, .description-block_13 p i {font-size:12px;}

.title-block_13 H3 A {color:#5e5e5e; font-weight:bold;}
.title-block_13 H3 A:hover {color:#3d3d3d; font-weight:bold;}

.element_13 div.right-block .description-block_13 p,.element_13 div.right-block .description-block_13 {
	margin:0px;
	padding:0px;
	font-weight:normal;
	font-size:14px;
	color:#555555;
}


.element_13 div.right-block .description-block_13 h1,
.element_13 div.right-block .description-block_13 h2,
.element_13 div.right-block .description-block_13 h3,
.element_13 div.right-block .description-block_13 h4,
.element_13 div.right-block .description-block_13 h5,
.element_13 div.right-block .description-block_13 h6,
.element_13 div.right-block .description-block_13 p, 
.element_13 div.right-block .description-block_13 strong,
.element_13 div.right-block .description-block_13 span {
	padding:2px !important;
	margin:0px !important;
}

.element_13 div.right-block .description-block_13 ul,
.element_13 div.right-block .description-block_13 li {
	padding:2px 0px 2px 5px;
	margin:0px 0px 0px 8px;
}

.element_13 div.right-block .price-block_13 {
    color: #0074a2;
}

.element_13 div.right-block .old-price {
	text-decoration: line-through;
        margin: 0px;
        padding: 0px;
        font-weight: normal;
        font-size: 15px;
        padding: 7px 10px 7px 10px;
        margin: 0px 10px 0px 0px;
        border-radius: 5px;
        color: #f9f9f9;
        background: #0074a2;
}

.element_13 div.right-block .old-price-block {

}

.element_13 div.right-block .discont-price-block {

}

.element_13 .button-block {
	position:relative;
}

.element_13 div.right-block .button-block a,.element_13 div.right-block .button-block a:link,.element_13 div.right-block .button-block a:visited {
	position:relative;
	display:inline-block;
	padding:6px 12px;
	background:#5e5e5e;
	color:#ffffff;
	font-size:14;
	text-decoration:none;
}

.element_13 div.right-block .button-block a:hover,.pupup-elemen.element div.right-block .button-block a:focus,.element_13 div.right-block .button-block a:active {
	background:#3D3D3D;
	color:#ffffff;
}



@media only screen and (max-width: 767px) {
	
	.element_13 > div {
		display:block;
		width:100%;
		clear:both;
	}

	.element_13 div.left-block_13 {
		padding-right:0px;
	}

	.element_13 div.left-block_13 .main-image-block_13 {
		clear:both;
		width:100%; 
	}

	.element_13 div.left-block_13 .thumbs-block ul {
		width:100%; 
	}
}

@media only screen and (max-width: 600px) {
       .element_13 div.left-block_13 .main-image-block_13 img {
              margin: 0px !important;
              padding: 0px !important;
              width: 100% !important;
              height: auto;
       }
}

#huge_it_catalog_content_13 #huge_it_catalog_options_13 {
        overflow: hidden;
    margin-top: 5px;
    float: none;
    width: 100%;
}

#huge_it_catalog_content_13 #huge_it_catalog_options_13 ul {
  margin: 0px !important;
  padding: 0px !important;
  list-style: none;
}

#huge_it_catalog_content_13 #huge_it_catalog_filters_13 ul {
  margin: 0px !important;
  padding: 0px !important;
  overflow: hidden;
  }

            #huge_it_catalog_content_13 #huge_it_catalog_options_13 ul {
                float: left;
            }
    
#huge_it_catalog_content_13 #huge_it_catalog_options_13 ul li {
    border-radius: 3pxpx;
    list-style-type: none;
    margin: 0px !important;
    float:left !important;margin: 4px 8px 4px 0px !important;float:left !important;border: 1px solid #ccc;}

#huge_it_catalog_content_13 #huge_it_catalog_options_13 ul li a {
    background-color: ##fff !important;
    font-size:12pxpx !important;
    color:##000 !important;
    text-decoration: none;
    cursor: pointer;
    margin: 0px !important;
    display: block;
    padding:3px;
}

/*#huge_it_catalog_content_13 #huge_it_catalog_options_13 ul li:hover {
    
}*/

#huge_it_catalog_content_13 #huge_it_catalog_options_13 ul li a:hover {
    background-color: ##fff !important;
    color:##000 !important;
    cursor: pointer;
}

#huge_it_catalog_content_13 #huge_it_catalog_filters_13 {
    margin-top: 5px;
    float: none;
   width: 100%;
    }

#huge_it_catalog_content_13 #huge_it_catalog_filters_13 ul li {
    list-style-type: none;
    float:left !important;margin: 4px 8px 4px 0px !important;float:left !important;border: 1px solid #ccc;}

#huge_it_catalog_content_13 #huge_it_catalog_filters_13 ul li a {
    font-size:12pxpx !important;
    color:##000 !important;
    background-color: ##fff !important;
    border-radius: 3pxpx;
    padding: 3px;
    display: block;
    text-decoration: none;
}

#huge_it_catalog_content_13 #huge_it_catalog_filters_13  ul li a:hover {
    color:##000 !important;
    background-color: ##fff !important;
    cursor: pointer;
}

#huge_it_catalog_content_13 section {
    position:relative;
    display:block;
}

#huge_it_catalog_content_13 #huge_it_catalog_container_13 {

    }

.catalog_pagination_block_13{
    /*text-align: center;*/
    padding: 20px 0px;
    margin: 16px 0px 35px 0px;
}
.catalog_pagination_13{
    text-align: center;
    /*display: inline-block;*/
    /*height: 50px;*/
/*    border: 1px solid #dadada;
    border-radius: 6px;*/
}
.catalog_pagination_13 a, .catalog_pagination_13 span{
    color: #515151;
    font-size: 20px;
    text-decoration: none;
    text-align: center;
/*    width: 48px;
    height: 48px;*/
    /*display: inline-block;*/
    /*float: left;*/
    margin: 0;
    padding: 0;
}
.catalog_pagination_13 .pagination-text{
    /*float: left;*/
    color: #000;
    font-size: 22px;
    /*font-weight: bold;*/
    padding: 12px 0px;
    text-decoration: none;
    text-align: center;
    /*display: inline-block;*/
/*    width: 180px;
    height: 48px;*/
    /*background-color: #fff;*/
}
@media only screen and (max-width:500px) {
	.catalog_pagination_13 .pagination-text{
            font-size: 16px !important;
            width: 120px !important;
	}
        .catalog_pagination_block_13{
            text-align: left;
        }
}
.catalog_pagination_13 a{
    text-align: center;
    position: relative;
    margin-right: 5px;
    
}
.catalog_pagination_13 a i{
    font-size: 22px;
    color: #000;
}
.catalog_pagination_13 .go-to-first{
    font-size: 10px !important;
}
.catalog_pagination_13 .go-to-first-passive{
    font-size: 10px !important;
}

.catalog_pagination_13 .go-to-previous{
    font-size: 10px !important;
}
.catalog_pagination_13 .go-to-previous-passive{
}

.catalog_pagination_13 .go-to-last{
    font-size: 10px !important;
}
.catalog_pagination_13 .go-to-last-passive{
    font-size: 10px !important;
}

.catalog_pagination_13 .go-to-next{
    font-size: 10px !important;
}
.catalog_pagination_13 .go-to-next-passive{
    font-size: 10px !important;
}

.zoomContainer {
    z-index: 10;
}
.catalog_load_block_13{
    margin: 35px 0px;
}
.catalog_load_13{
    text-align: center;
}
.catalog_load_13 a{
    text-decoration: none;
    /*width: 100%;*/
    border-radius: 5px;
    display: inline-block;
    padding: 5px 15px;
    font-size: 20px !important;
    color: #F2F2F2 !important;
    background: #A1A1A1 !important;
    cursor: pointer;
}
.catalog_load_13 a:hover{
    color: #FFFFFF !important;
    background: #A1A1A1 !important;
}
.catalog_load_13 a:focus{
    outline: none;
}

.assh1 {float:left; color:#262626; padding:5px; background: #f2f2f2; height: 35px; width:145px; font-size:18px; text-align:center; margin-right:20px;}
.assh2 {float:left}
.assh3 {float:left}
.assh4 {float:left}

.assh5 {float:left; color:#b32909; padding:5px; background: #FFF; height: 35px; width:200px; font-size:12px; text-align:center; margin-right:20px;}

.assh6 {float:left; color:#000; padding:5px; background: #FEBD69; border:1px solid #000; border-radius:5px; height: 35px; width:150px; font-size:18px; text-align:center; margin-right:20px;}

@media only screen and (max-width: 480px) {
	.assh1 {margin-left:25%; margin-bottom:10px;}
	.assh2 {margin-left:15%;}
	.assh3 {margin-left:17%;}
	.assh4 {margin-left:10%; margin-bottom:10px; margin-top:10px;}
	.assh5 {margin-left:17%;}
	.assh6 {margin-left:24%;}
	.byw {display:none}
}
</style>


<?php 

$site_url = get_bloginfo('url');

$cats = get_the_category();

$breadcrumb =  '';

foreach($cats as $cat)
{
	$breadcrumb .= '<a href="'.get_category_link($cat->term_id).'">'.$cat->name.'</a> / ';
}

if(isset($cats[0]->parent) && $cats[0]->parent)
{
	$par = get_the_category_by_ID($cats[0]->parent);
	$breadcrumb = '<a href="'.$cats[0]->parent.'">'.$par.'</a> / '.$breadcrumb;
}

$breadcrumb = '<a href="'.$site_url.'">'.pll__('Homepage').'</a> / '.$breadcrumb;

print trim($breadcrumb, ' / ');
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			
				
	<div class="element_13 catalog_ccolorbox_grouping_13  hugeitmicro-item">
                              
			<div class="left-block_13">
			<div class="main-image-block_13 main-image-block">
			<a href="<?php echo get_the_post_thumbnail_url(); ?>" rel="lightbox[roadtrip<?php echo get_the_ID(); ?>]"><img src="<?php echo get_the_post_thumbnail_url(); ?>"></a>
			</div>
                                                                                                     
				<div class="thumbs-block">				
			
					<ul class="thumbs-list_13">
					<?php
	$out_of_stock = get_field('out_of_stock'); 
    $images = acf_photo_gallery('photos', get_the_ID());

    if( count($images) ):
     
        foreach($images as $image):
            $id = $image['id']; // The attachment id of the media
            $title = $image['title']; //The title
            $caption= $image['caption']; //The caption
            $full_image_url = $image['full_image_url']; //Full size image url
			$real_image_url = $full_image_url;
            $full_image_url = acf_photo_gallery_resize_image($full_image_url, 262, 160); //Resized size to 262px width by 160px height image url
            $thumbnail_image_url= $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
            $url= $image['url']; //Goto any link when clicked
            $target= $image['target']; //Open normal or new tab
            $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
            $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
?>

        <?php if( !empty($url) ){ ?><a href="<?php echo $url; ?>" <?php echo ($target == 'true' )? 'target="_blank"': ''; ?>><?php } ?>
            <li><a href="<?php echo $real_image_url; ?>" rel="lightbox[roadtrip<?php echo get_the_ID(); ?>]"><img src="<?php echo $full_image_url; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>"></a></li>
        <?php if( !empty($url) ){ ?></a><?php } ?>
<?php endforeach; endif; ?>																						                                                                                            			
					</ul>			
					
			</div>
		</div>
		
		<div class="right-block">
		
            <div class="title-block_13"><h3><?php the_title(); ?></h3></div>                     

			<?php 
				$isFreeShT = get_field('free_shipping');
				if(isset($isFreeShT[0]) && $isFreeShT[0] == 'Yes')
					$isFreeSh = true;
				else
					$isFreeSh = false;
				$price = get_field('price');
				$weight = trim(get_field('weight'));
				$shipping_default = get_field('shipping_default');
				$item_number = get_field('item_number');
				$shipser1 = get_option('shipser1');
				$shipser2 = get_option('shipser2');
			?>
			
			<div style="display:table; margin-left:5px;">
			<!-- shipping otions-->
				<?php 
				if(!isset($_SESSION['countryCode']))
				{
					$userIpData = getCountryByIP(); 
					$countryCode = $userIpData['countryCode'];
					$_SESSION['countryCode'] = $countryCode;
				}
				else
				{
					$countryCode = $_SESSION['countryCode'];
				}
			
				global $wpdb;
				$result = $wpdb->get_results('SELECT region FROM countries WHERE iso2="'.$countryCode.'"', ARRAY_A);
				$region_list = array('zone1','zone2','zone3','zone4','zone5','zone6','zone7','zone8','zone9','other');
				if(isset($result[0]['region']) && !empty($result[0]['region']))
				{
					$region = $result[0]['region'];
					if($weight != ''  && in_array($region, $region_list))
					{

						$exPT = $wpdb->get_results('SELECT max(z1.'.$region.') AS ex FROM wp_shipping_zones AS z1 WHERE z1.weight <= '.$weight, ARRAY_A);
						$cheapPT = $wpdb->get_results('SELECT max(z2.'.$region.') AS cheap FROM wp_shipping_zones2 AS z2 WHERE z2.weight <= '.$weight, ARRAY_A);
						$shipprice[0]['ex'] = $exPT[0]['ex'];
						$shipprice[0]['cheap'] = $cheapPT[0]['cheap'];
					}
					else
					{
						$shipprice[0]['ex'] = $shipping_default;
						$shipprice[0]['cheap'] = $shipping_default;
					}
				}
				else
				{
					$region = '';
				}
					
				
				
				?>
				<div  style="display:table; margin-bottom:10px;">
					<?php
					if($price != ''){
					?>
					<div class="assh1"><?php echo $price; ?> $</div>
					<?php } ?>
					<?php 
					if(isset($out_of_stock[0]) && $out_of_stock[0] == 'Yes' && isset($countryCode) && in_array($countryCode, getAmazonctrs())) {
						$amazon_link = get_field('amazon_link');
						$buyamazon = get_option('buyamazon');						
						echo '<div class="assh5">To deliver the desired tools as soon as possible, we opened a store on Amazon. This way We Save Your Time. Just click the orange button to check the price</div>';
						echo '<div class="assh6"><a href="'.$amazon_link.'">'.$buyamazon.'</a></div>';
						?>
					<?php } else { ?>
					<div class="assh2"><?php echo do_shortcode('[wp_cart_button name="'.get_the_title().'" price="'.$price.'" item_number="'.$item_number.'"]'); ?></div>
					<?php }?>
				</div>
				<div style="display:table;">
					<div class="assh3">Ship to: 
						<select name="countries" id="countries" style="width:150px;">
							<?php 
								
								
								echo generateCountryDropDown($countryCode); ?>
						</select>
					</div>

						<?php if($region != '' && !$isFreeSh) {
							
						if(isset($_SESSION['ship_mode']))
						{
							if($_SESSION['ship_mode'] == 'ex')
							{
								$ex_sel = 'selected';
								$ch_sel = '';
							}
							else
							{
								$ex_sel = '';
								$ch_sel = 'selected';
							}
						}
						else
						{
							$ch_sel = 'selected';
							$ex_sel = '';
						}
						
						$speed = $wpdb->get_results('SELECT slow, fast FROM speed WHERE zone="'.$region.'"', ARRAY_A);
						
						?>
						<div class="assh4">
							<span class="byw">&#160;&#160;by&#160;&#160;</span><select name="speedB" id="speedB">
								<option value="ex" <?php echo $ex_sel;?>><?php echo $shipser1; ?> (<?php echo $speed[0]['fast']; ?>) | <?php echo printShPrice($shipprice[0]['ex']); ?></option>
								<option value="cheap" <?php echo $ch_sel;?>><?php echo $shipser2; ?> (<?php echo $speed[0]['slow']; ?>) | <?php echo printShPrice($shipprice[0]['cheap']); ?></option>
							</select>
						</div>
						<?php } else { echo printShPrice($shipping_default); }?>
					</div>
			</div>
			
			<div class="description-block_13" style="padding-top:30px; border-top: 1px solid #EEE;"><?php the_content(); ?></div>
			
			
			<script>
jQuery(document).ready(function() {
	jQuery('#countries').on('change', function() {
  jQuery.ajax({
			type: "POST",
			data: 'action=counup&countryCode='+this.value,
			url: "https://beavercrafttools.com/wp-admin/admin-ajax.php",
			success: function(data)
			{   
			location.reload();
			}
		});
});

jQuery('#speedB').on('change', function() {
  jQuery.ajax({
			type: "POST",
			data: 'action=shipup&ship_mode='+this.value,
			url: "https://beavercrafttools.com/wp-admin/admin-ajax.php",
			success: function(data)
			{   
			location.reload();
			}
		});
});
})
</script>
		<?php 
			$post_type = get_post_type();
			if($post_type == 'product')
			{
				$ccforB = get_the_category(); ?>
				<p></p>
				<div class="button-block"><a href="javascript:history.back();"><?php print pll__('Back'); ?></a></div>
            <?php } ?>                                    
        </div>
		
	</div>

		</article>

		<div class="clearboth"></div>