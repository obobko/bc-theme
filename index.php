<?php
/**
 * The main template file.
 *
 * @package ThinkUpThemes
 */
 
 get_header(); 
 
 if(!isset($_GET['single_prod_id']))
 {
 ?>
 
 <h2 align="right"><?php pll_e('new_goods'); ?><h2>
 
 <hr>							
 
<!--Категории товаров, выводятся из allcategories.html-->	
 <div id="container">	 
 <?php echo ""; include get_template_directory().'/allcategories.html'; ?>
 </div>
		
 <div id="container">	 
 
	 <!-- Блок с новинками. Указана несуществующая категория, чтоб не выводились никакие товары после блока категорий товаров. Если нужно включить, указать категорию cat=18 или другую, если есть специальная для этих целей -->
	 <?php query_posts('cat=0001&post_type=product&posts_per_page=3'); 
	 
	 while( have_posts() ): the_post(); ?>					
	 
		 <div class="blog-grid element<?php consulting_thinkup_input_stylelayout(); ?>">					
		 
			 <article id="post-<?php the_ID(); ?>" <?php post_class('blog-article'); ?>>						
				 <?php if( has_post_thumbnail() ) { ?>						
				 
					 <header class="entry-header">							
					 
						<?php echo consulting_thinkup_input_blogimage(); ?>							
					 
					 </header>						
				 
				 <?php } ?>						
				 
					 <div class="entry-content">							
					 
						 <?php consulting_thinkup_input_blogtitle(); ?>							
				
					 
					 </div>
				 
				 <div class="clearboth"></div>

			 </article>
		 
		 <!-- #post-<?php get_the_ID(); ?> -->					
		 
		 </div>				
	 
	 <?php endwhile; wp_reset_query(); ?>								
 
 </div>
 
 <div class="clearboth"></div>
 
 <h2 align="right"><?php pll_e('news'); ?><h2>
 
 <hr>		
 
 <div id="container">				
 
	 <!-- Блок с новостями -->
	 <?php query_posts('cat=20&posts_per_page=3'); 
	 
	 while( have_posts() ): the_post(); ?>					
	 
		 <div class="blog-grid element<?php consulting_thinkup_input_stylelayout(); ?>">					
		 
			 <article id="post-<?php the_ID(); ?>" <?php post_class('blog-article'); ?>>						
			 
				 <?php if( has_post_thumbnail() ) { ?>						
				 
					 <header class="entry-header">							
					 
						<a href="<?php the_permalink(); ?>"><?php echo consulting_thinkup_input_blogimage(); ?></a>									
					 
					 </header>						
				 
				 <?php } ?>						
				 
				 <div class="entry-content">							
				 
					 <?php consulting_thinkup_input_blogtitle(); ?>												
				 
				 </div>
				 
				 <div class="clearboth"></div>					
			 
			 </article>
			 
			 <!-- #post-<?php get_the_ID(); ?> -->					
		 
		 </div>				
	 
	 <?php endwhile; wp_reset_query(); ?>								
 
 </div>
 
 <div class="clearboth"></div>	
 
  <div class="blog-grid element" style="margin-top:30px;">
  <h2 align="right">SUBSCRIBE TO OUR NEWSLETTER</h2>
  <hr>
  <div class="mailchform">
				<?php 
				echo htmlspecialchars_decode(get_option('substxtbox'));
				?>
			</div>
  </div>
<?php 
 } else 
 
 { 
get_template_part( 'content', 'single' );
 }
?> 
 <?php get_footer() ?>

