<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id="main-core".
 *
 * @package ThinkUpThemes
 */
?>

		</div><!-- #main-core -->
		</div><!-- #main -->
		<?php /* Sidebar */ consulting_thinkup_sidebar_html(); ?>
	</div>
	</div><!-- #content -->

	<footer>
		<?php /* Custom Footer Layout */ consulting_thinkup_input_footerlayout();
		echo	'<!-- #footer -->';  ?>
		
		<div id="sub-footer">

		<div id="sub-footer-core">
		
			<!-- <div class="copyright"> -->
			<p style="color: #808080; font-weight: 800;">Beaver Craft ® 2011-<?php echo date('Y'); ?>          <a style="color: #808080; text-decoration:underline;" href="<?php echo site_url(); ?>/privacy-policy/">Privacy policy</a>     <a style="color: #808080; text-decoration:underline;" href="<?php echo site_url(); ?>/terms-of-use/">Terms of use</a></p>
			<!-- <?php /* === Add custom footer === */ consulting_thinkup_input_copyright(); ?> -->   
			</div>
			<!-- .copyright -->
			
			<!--OLD FOOTER <div class="copyright"> 
			<p style="color: #808080; font-weight: 800;">Beaver Craft ® 2011-<?php echo date('Y'); ?></p><p><a style="color: #808080; text-decoration:underline;" href="<?php echo site_url(); ?>/privacy-policy/">Privacy policy</a></p><p><a style="color: #808080; text-decoration:underline;" href="<?php echo site_url(); ?>/terms-of-use/">Terms of use</a></p>
			<!-- <?php /* === Add custom footer === */ consulting_thinkup_input_copyright(); ?>    
			</div> -->
			
			

			<?php if ( has_nav_menu( 'sub_footer_menu' ) ) : ?>
			<?php wp_nav_menu( array( 'depth' => 1, 'container_class' => 'sub-footer-links', 'container_id' => 'footer-menu', 'theme_location' => 'sub_footer_menu' ) ); ?>
			<?php endif; ?>
			<!-- #footer-menu -->

		</div>
		</div>
	</footer><!-- footer -->
	<?php 
	if(isset($_SESSION['simpleCart']))
		$simpleCart = json_encode($_SESSION['simpleCart']);
	else
		$simpleCart = json_encode(array());
	?>
	<script>
	/*function catchdata() {
       formdata = jQuery('#paypalform').serialize();
		   jQuery.ajax({
			type: "POST",
			data: 'action=my_action&'+formdata+'&cart=<?php echo json_encode($_SESSION['simpleCart']);?>',
			url: "<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
			success: function(data)
			{   
				jQuery('#paypalform').submit();
			}
		});
	}*/
	<?php 
	if(is_page(2131))
	{		?>
		function autoSave()
		{
		  formdata = jQuery('#paypalform').serialize();
		   jQuery.ajax({
			type: "POST",
			data: 'action=my_action&'+formdata+'&cart=<?php echo $simpleCart;?>',
			url: "<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
			success: function(data)
			{   
			//alert("saved");
			}
		});
		  setTimeout("autoSave()", 6000); // Autosaves every minute.
		}
		autoSave();
		
		
	jQuery(document).on('submit','#paypalform',function(){
		formdata = jQuery('#paypalform').serialize();
		   jQuery.ajax({
			type: "POST",
			data: 'action=my_action&'+formdata+'&cart=<?php echo $simpleCart;?>',
			url: "<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
			success: function(data)
			{   
			}
		});
	});
	<?php }?>
	
	jQuery( document ).ready(function(){
		var prod = '';
		jQuery('.buybut').each(function () {
			prod += ',' + jQuery(this).attr('data-row');
		});
		
		jQuery.ajax({
				type: "POST",
				data: 'action=getbuybutt&id='+prod,
				url: "<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
				success: function(data)
				{   
					data = jQuery.parseJSON(data);
					 jQuery.each(data, function (key, value) {
						jQuery('#prod_'+key).html(value);
					});
				}
			});
	});
	
	jQuery( document ).ready(function(){
		var prod = jQuery('.shipbut').attr('data-row');
		
		jQuery.ajax({
				type: "POST",
				data: 'action=getshipmentdrop&id='+prod,
				url: "<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
				success: function(data)
				{   
					jQuery('#ship_'+prod).html(data);
				}
			});
	});
	
	jQuery( document ).ready(function(){
            //Perform Ajax request.
            jQuery.ajax({
                url: "<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
                type: "POST",
				data: 'action=getcartbut',
				async: true,
                success: function(data){
                    jQuery('#dincart').html(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                    jQuery('#dincart').html(errorMsg);
                  }
            });
        });

	</script>


<?php wp_footer(); ?>

</body>
</html>