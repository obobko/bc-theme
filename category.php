<?php
/**
 * The template for displaying Archive pages.
 *
 * @package ThinkUpThemes
 */

get_header(); 
$lang = pll_current_language();
if($lang == 'uk')
	$catL = get_option('option_1');
else
	$catL = get_option('option_2');

?>
<style type="text/css">

/*.zoomLens {
    width: 100px !important;
    height: 100px !important;*/
/*}*/

.element_13 {
	position: relative;
	width:93%; 
	margin:5px 0px 5px 0px;
	padding:2%;
	clear:both;
	overflow: hidden;
	/*border:1px solid #dedede;
	background:#f9f9f9;*/
}

.element_13 > div {
	display:table-cell;
}

.element_13 div.left-block_13 {
	padding-right:10px;
}

.element_13 div.left-block_13 .main-image-block_13 {
	clear:both;
	width:240px; 
}

.element_13 div.left-block_13 .main-image-block_13 img {
	margin:0px !important;
	padding:0px !important;
	width:240px !important; 
	height:auto;
}

.element_13 div.left-block_13 .thumbs-block {
	position:relative;
	margin-top:10px;
}

.element_13 div.left-block_13 .thumbs-block ul {
	width:240px; 
	height:auto;
	display:table;
	margin:0px;
	padding:0px;
	list-style:none;
}

.element_13 div.left-block_13 .thumbs-block ul li {
	margin:0px 3px -22px 2px;
	padding:0px;
	width:75px; 
	height:75px; 
	float:left;
}

.element_13 div.left-block_13 .thumbs-block ul li a {
	display:block;
	width:75px; 
	height:75px; 
}

.element_13 div.left-block_13 .thumbs-block ul li a img {
	/*margin:0px !important;
	padding:0px !important;
	width:75px; 
	height:75px;*/ 
}

.element_13 div.right-block {
	vertical-align:top;
	width:100%;
}

.title-block_13 H3 A {color:#5e5e5e; font-weight:bold;}
.title-block_13 H3 A:hover {color:#3d3d3d; font-weight:bold;}

.description-block_13 em, .description-block_13 i, .description-block_13 p em, .description-block_13 p i {font-size:12px;}

.element_13 div.right-block > div {
	width:100%;
	padding-bottom:10px;
	margin-top:10px;
			background:url('http://beavercraft.com.ua/wp-content/plugins/product-catalog/Front_end/../images/divider.line.png') center bottom repeat-x;
		
}

.element_13 div.right-block > div:last-child {
	background:none;
}

.element_13 div.right-block .title-block_13  {
	margin-top:-4px;
}

.element_13 div.right-block .title-block_13 h3 {
	margin:0px;
	padding:0px;
	font-weight:normal;
	font-size:18px !important;
	line-height:22px !important;
	color:#0074a2;
}

.element_13 div.right-block .description-block_13 p,.element_13 div.right-block .description-block_13 {
	margin:0px;
	padding:0px;
	font-weight:normal;
	font-size:14px;
	color:#555555;
}


.element_13 div.right-block .description-block_13 h1,
.element_13 div.right-block .description-block_13 h2,
.element_13 div.right-block .description-block_13 h3,
.element_13 div.right-block .description-block_13 h4,
.element_13 div.right-block .description-block_13 h5,
.element_13 div.right-block .description-block_13 h6,
.element_13 div.right-block .description-block_13 p, 
.element_13 div.right-block .description-block_13 strong,
.element_13 div.right-block .description-block_13 span {
	padding:2px !important;
	margin:0px !important;
}

.element_13 div.right-block .description-block_13 ul,
.element_13 div.right-block .description-block_13 li {
	padding:2px 0px 2px 5px;
	margin:0px 0px 0px 8px;
}

.element_13 div.right-block .price-block_13 {
    color: #0074a2;
}

.element_13 div.right-block .old-price {
	text-decoration: line-through;
        margin: 0px;
        padding: 0px;
        font-weight: normal;
        font-size: 15px;
        padding: 7px 10px 7px 10px;
        margin: 0px 10px 0px 0px;
        border-radius: 5px;
        color: #f9f9f9;
        background: #0074a2;
}

.element_13 div.right-block .old-price-block {

}

.element_13 div.right-block .discont-price-block {

}

.element_13 .button-block {
	position:relative;
}

.element_13 div.right-block .button-block a,.element_13 div.right-block .button-block a:link,.element_13 div.right-block .button-block a:visited {
	position:relative;
	display:inline-block;
	padding:6px 12px;
	background:#5e5e5e;
	color:#ffffff;
	font-size:14;
	text-decoration:none;
}

.element_13 div.right-block .button-block a:hover,.pupup-elemen.element div.right-block .button-block a:focus,.element_13 div.right-block .button-block a:active {
	background:#3D3D3D;
	color:#ffffff;
}



@media only screen and (max-width: 767px) {
	
	.element_13 > div {
		display:block;
		width:100%;
		clear:both;
	}

	.element_13 div.left-block_13 {
		padding-right:0px;
	}

	.element_13 div.left-block_13 .main-image-block_13 {
		clear:both;
		width:100%; 
	}

	.element_13 div.left-block_13 .thumbs-block ul {
		width:100%; 
	}
}

@media only screen and (max-width: 600px) {
       .element_13 div.left-block_13 .main-image-block_13 img {
              margin: 0px !important;
              padding: 0px !important;
              width: 100% !important;
              height: auto;
       }
}

#huge_it_catalog_content_13 #huge_it_catalog_options_13 {
        overflow: hidden;
    margin-top: 5px;
    float: none;
    width: 100%;
}

#huge_it_catalog_content_13 #huge_it_catalog_options_13 ul {
  margin: 0px !important;
  padding: 0px !important;
  list-style: none;
}

#huge_it_catalog_content_13 #huge_it_catalog_filters_13 ul {
  margin: 0px !important;
  padding: 0px !important;
  overflow: hidden;
  }

            #huge_it_catalog_content_13 #huge_it_catalog_options_13 ul {
                float: left;
            }
    
#huge_it_catalog_content_13 #huge_it_catalog_options_13 ul li {
    border-radius: 3pxpx;
    list-style-type: none;
    margin: 0px !important;
    float:left !important;margin: 4px 8px 4px 0px !important;float:left !important;border: 1px solid #ccc;}

#huge_it_catalog_content_13 #huge_it_catalog_options_13 ul li a {
    background-color: ##fff !important;
    font-size:12pxpx !important;
    color:##000 !important;
    text-decoration: none;
    cursor: pointer;
    margin: 0px !important;
    display: block;
    padding:3px;
}

/*#huge_it_catalog_content_13 #huge_it_catalog_options_13 ul li:hover {
    
}*/

#huge_it_catalog_content_13 #huge_it_catalog_options_13 ul li a:hover {
    background-color: ##fff !important;
    color:##000 !important;
    cursor: pointer;
}

#huge_it_catalog_content_13 #huge_it_catalog_filters_13 {
    margin-top: 5px;
    float: none;
   width: 100%;
    }

#huge_it_catalog_content_13 #huge_it_catalog_filters_13 ul li {
    list-style-type: none;
    float:left !important;margin: 4px 8px 4px 0px !important;float:left !important;border: 1px solid #ccc;}

#huge_it_catalog_content_13 #huge_it_catalog_filters_13 ul li a {
    font-size:12pxpx !important;
    color:##000 !important;
    background-color: ##fff !important;
    border-radius: 3pxpx;
    padding: 3px;
    display: block;
    text-decoration: none;
}

#huge_it_catalog_content_13 #huge_it_catalog_filters_13  ul li a:hover {
    color:##000 !important;
    background-color: ##fff !important;
    cursor: pointer;
}

#huge_it_catalog_content_13 section {
    position:relative;
    display:block;
}

#huge_it_catalog_content_13 #huge_it_catalog_container_13 {

    }

.catalog_pagination_block_13{
    /*text-align: center;*/
    padding: 20px 0px;
    margin: 16px 0px 35px 0px;
}
.catalog_pagination_13{
    text-align: center;
    /*display: inline-block;*/
    /*height: 50px;*/
/*    border: 1px solid #dadada;
    border-radius: 6px;*/
}
.catalog_pagination_13 a, .catalog_pagination_13 span{
    color: #515151;
    font-size: 20px;
    text-decoration: none;
    text-align: center;
/*    width: 48px;
    height: 48px;*/
    /*display: inline-block;*/
    /*float: left;*/
    margin: 0;
    padding: 0;
}
.catalog_pagination_13 .pagination-text{
    /*float: left;*/
    color: #000;
    font-size: 22px;
    /*font-weight: bold;*/
    padding: 12px 0px;
    text-decoration: none;
    text-align: center;
    /*display: inline-block;*/
/*    width: 180px;
    height: 48px;*/
    /*background-color: #fff;*/
}
@media only screen and (max-width:500px) {
	.catalog_pagination_13 .pagination-text{
            font-size: 16px !important;
            width: 120px !important;
	}
        .catalog_pagination_block_13{
            text-align: left;
        }
}
.catalog_pagination_13 a{
    text-align: center;
    position: relative;
    margin-right: 5px;
    
}
.catalog_pagination_13 a i{
    font-size: 22px;
    color: #000;
}
.catalog_pagination_13 .go-to-first{
    font-size: 10px !important;
}
.catalog_pagination_13 .go-to-first-passive{
    font-size: 10px !important;
}

.catalog_pagination_13 .go-to-previous{
    font-size: 10px !important;
}
.catalog_pagination_13 .go-to-previous-passive{
}

.catalog_pagination_13 .go-to-last{
    font-size: 10px !important;
}
.catalog_pagination_13 .go-to-last-passive{
    font-size: 10px !important;
}

.catalog_pagination_13 .go-to-next{
    font-size: 10px !important;
}
.catalog_pagination_13 .go-to-next-passive{
    font-size: 10px !important;
}

.zoomContainer {
    z-index: 10;
}
.catalog_load_block_13{
    margin: 35px 0px;
}
.catalog_load_13{
    text-align: center;
}
.catalog_load_13 a{
    text-decoration: none;
    /*width: 100%;*/
    border-radius: 5px;
    display: inline-block;
    padding: 5px 15px;
    font-size: 20px !important;
    color: #F2F2F2 !important;
    background: #A1A1A1 !important;
    cursor: pointer;
}
.catalog_load_13 a:hover{
    color: #FFFFFF !important;
    background: #A1A1A1 !important;
}
.catalog_load_13 a:focus{
    outline: none;
}

.assh1 {float:left; color:#262626; padding:5px; background: #f2f2f2; height: 35px; width:145px; font-size:18px; text-align:center; margin-right:20px;}
.assh2 {float:left}

.assh5 {float:left; color:#b32909; padding:5px; background: #FFF; height: 100%; width:200px; font-size:12px; text-align:center; margin-right:20px;}

.assh6 {float:left; color:#000; padding:5px; background: #FEBD69; border:1px solid #000; border-radius:5px; height: 35px; width:150px; font-size:18px; text-align:center; margin-right:20px;}

@media only screen and (max-width: 480px) {
	.assh1 {margin-left:25%; margin-bottom:10px;}
	.assh2 {margin-left:15%;}
	.assh5 {margin-left:17%;}
	.assh6 {margin-left:24%;}
}
</style>
<?php

$currentCatID = get_query_var( 'cat' );
$currentCat = get_category($currentCatID);

$currentSubCat = get_categories(array('child_of' => $currentCatID));

$subcats = array();
if(isset($currentSubCat) && !empty($currentSubCat))
{
	$i = 0;
	foreach($currentSubCat AS $csc)
	{
		$subcats[$i] = $csc->term_id;
		$i++;
	}
}

array_push($subcats, $currentCatID);

if(isset($currentCat->parent) && $currentCat->parent !=0)
{
	$currentCatParent = get_category($currentCat->parent);
}

if($currentCatID == $catL)
{
	$topCatID = $currentCatID;
}
elseif($currentCat->parent == $catL)
{
	$topCatID = $currentCat->parent;
}
elseif($currentCatParent->parent == $catL)
{
	$topCatID = $currentCatParent->parent;
}

/** Задается вывод категорий. Было по дате, стало по возрастанию в slug'e **/	
$args = array('child_of' =>$catL, 'orderby' => 'slug', 'hierarchical' => 0 );
	$categories = get_categories( $args );
	
if(!isset($_SESSION['countryCode']))
{
	$userIpData = getCountryByIP(); 
	$countryCode = $userIpData['countryCode'];
	$_SESSION['countryCode'] = $countryCode;
}
else
{
	$countryCode = $_SESSION['countryCode'];
}

?>

<article id="post-13" class="post-13 page type-page status-publish hentry">

				<div class="elementor elementor-13">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
							<section class="elementor-element elementor-element-otdhdrn elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				
				<?php foreach($categories as $category) {
					if($category->parent != $catL) continue;
					?>
				<div class="elementor-element elementor-element-mmjoqcm elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="zuysnwt" class="elementor-element elementor-element-zuysnwt elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
					<figure class="wp-caption">
						<a href="<?php print get_category_link($category->term_id);?>" class="elementor-clickable" data-elementor-open-lightbox="default">
		<img src="<?php print cfix_featured_image_url( array( 'size' => 'large', 'cat_id' => $category->term_id ) ); ?>" class="elementor-animation-hang attachment-large size-large"/>				</a>
						<figcaption class="widget-image-caption wp-caption-text"><?php print $category->name;?></figcaption>
					</figure>
				</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<?php } ?>
				
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
				
		</article>
		
		<?php 
					if($currentCat->parent == $catL || (isset($currentCatParent) && $currentCatParent->parent==$catL))
					{
						?>
						<div class="elementor-row">						
						<?php
						$subCategories = get_categories(array('child_of' =>$currentCat->term_id, 'hide_empty'  => 0));
						if(!count($subCategories) && ($currentCat->parent != $catL))
								$subCategories = get_categories(array('child_of' =>$currentCat->parent, 'hide_empty'  => 0));
						foreach($subCategories as $subCategory) {
						?>
						<div class="elementor-element elementor-element-mmjoqcm elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="zuysnwt" class="elementor-element elementor-element-zuysnwt elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
					<figure class="wp-caption">
						<a href="<?php print get_category_link($subCategory->term_id);?>" class="elementor-clickable" data-elementor-open-lightbox="default">
		<img src="<?php print cfix_featured_image_url( array( 'size' => 'underb', 'cat_id' => $subCategory->term_id ) ); ?>" class="elementor-animation-hang attachment-large size-large"/>				</a>
						<figcaption class="widget-image-caption wp-caption-text"><?php print $subCategory->name;?></figcaption>
					</figure>
				</div>
				</div>
				</div>
						</div>
			</div>
		</div>
					<?php
						}
						?>
						</div>
						<?php
					}
					
					if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }
					$the_query = new WP_Query( array( 'category__in' => $subcats, 'post_type' => 'product',  'posts_per_page' => 30, 'paged' => $paged) );

					// Цикл WordPress
					if( $the_query->have_posts() ){ 
		  while( $the_query->have_posts() ){ 
			  $the_query->the_post();
							   ?>
							   <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			
				
	<div class="element_13 catalog_ccolorbox_grouping_13  hugeitmicro-item">
                              
			<div class="left-block_13">			
			<div class="main-image-block_13 main-image-block">
			<a href="<?php echo get_the_post_thumbnail_url(); ?>" data-fancybox="gallery_<?php echo get_the_ID(); ?>" data-caption="<?php echo $title; ?>"><img src="<?php echo get_the_post_thumbnail_url(); ?>" class="lazy lazy-hidden" src="//beavercrafttools.com/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif" data-lazy-type="image" data-src="<?php echo get_the_post_thumbnail_url(); ?>"><noscript><img src="<?php echo get_the_post_thumbnail_url(); ?>"></noscript></a>
			</div>
                                                                                                     
				<div class="thumbs-block">				
			
					<ul class="thumbs-list_13">
					<?php
 
    $images = acf_photo_gallery('photos', get_the_ID());

    if( count($images) ):
     
        foreach($images as $image):
            $id = $image['id']; // The attachment id of the media
            $title = $image['title']; //The title
            $caption= $image['caption']; //The caption
            $full_image_url= $image['full_image_url']; //Full size image url
			$real_image_url =  $full_image_url;
            $full_image_url = acf_photo_gallery_resize_image($full_image_url, 262, 160); //Resized size to 262px width by 160px height image url
            $thumbnail_image_url= $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
            $url= $image['url']; //Goto any link when clicked
            $target= $image['target']; //Open normal or new tab
            $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
            $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
?>

        <?php if( !empty($url) ){ ?><a href="<?php echo $url; ?>" <?php echo ($target == 'true' )? 'target="_blank"': ''; ?>><?php } ?>
            <li><a href="<?php echo $real_image_url; ?>" data-fancybox="gallery_<?php echo get_the_ID(); ?>" data-caption="<?php echo $title; ?>"><img src="<?php echo $full_image_url; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>" class="lazy lazy-hidden" src="//beavercrafttools.com/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif" data-lazy-type="image" data-src="<?php echo $full_image_url; ?>"><noscript><img src="<?php echo $full_image_url; ?>"></noscript></a></li>
        <?php if( !empty($url) ){ ?></a><?php } ?>
<?php endforeach; endif; ?>																						                                                                                            			
					</ul>			
					
			</div>
		</div>
		
		<div class="right-block">
		
            <div class="title-block_13"><h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3></div>                                                                         
			<div style="display:table; margin-left:5px;" class="buybut" id="prod_<?php echo get_the_ID();?>" data-row="<?php echo get_the_ID();?>">
			</div>
			<div class="description-block_13"><?php the_content(); ?></div>
			
			<div class="button-block"><a href="<?php the_permalink(); ?>"><?php print pll__('More'); ?></a></div>
                                                                                                            
                                                                            
        </div>
		
	</div>

		</article>
<div style="width:100%; height:2px; background: #D0D0D0;"></div>
		<div class="clearboth"></div>
							   <?php
						  }
						  ?>
						  <div class="paging">
						  </br>
					 <?php
					 
					 
					 $slugForPage =  $currentCat->slug;
					 
					 if($currentCat->parent)
					 {
						$parentSlug = get_category($currentCat->parent);
						$slugForPage = $parentSlug->slug.'/'.$slugForPage;
					 }
					
					 
					 $total_pages = $the_query->max_num_pages;
					if($total_pages > 1)
					{
						for($i = 1; $i <= $total_pages; $i++)
						{
							if($i == 1)
								$pTail = '';
							else
								$pTail = '?page='.$i;
							echo '<a class="page-numbers" href="'.site_url().'/category/'.$slugForPage.'/'.$pTail.'">     '.$i.'</a> ';
						}
					}
					
				?>
				</div>
						  <?php
						  wp_reset_query();
						  
						  
					} else {
					  ?>
					  Category is empty
					  <?php
					}
					
					
						
					
				?>
<?php get_footer() ?>