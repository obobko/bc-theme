<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 *
 * @package ThinkUpThemes
 */

get_header(); 
if(isset($_SESSION['wpspsc_applied_coupon_code']))
{
	$coupon = $_SESSION['wpspsc_applied_coupon_code'];
	$myobj = get_option('wpspsc_coupons_collection');
	$myobj->markasused_by_code($coupon);
}

if(isset($_SESSION['simpleCart']))
	unset($_SESSION['simpleCart']);
if(isset($_SESSION['order_id']))
	unset($_SESSION['order_id']);
?>
<div style="padding-top:175px; padding-bottom:200px;">
	<center>
		<img style="width:260px; height:260px;" src="<?php echo get_template_directory_uri(); ?>/pk.png">
		<h1>Thank you for your purchase!</h1>
	</center>
</div>
<?
get_footer(); ?>