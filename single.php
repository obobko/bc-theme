<?php
/**
 * The Template for displaying all single posts.
 *
 * @package ThinkUpThemes
 */

get_header(); ?>
			<?php while ( have_posts() ) : the_post(); 
			
			if(get_post_type() == 'post')
				get_template_part( 'content', 'single' ); 
			else
				get_template_part( 'content', 'product' ); 
				
			?>

				<?php //wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'consulting' ), 'after'  => '</div>', ) ); ?>

				<?php //consulting_thinkup_input_nav( 'nav-below' ); ?>

				<?php /* Add comments */ consulting_thinkup_input_allowcomments(); ?>

			<?php endwhile; ?>

<?php get_footer(); ?>