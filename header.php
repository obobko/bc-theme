<?php 
header("Cache-Control: no cache");
session_cache_limiter("private_no_expire");

//print_r($_SESSION);
?>
<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<?php

if(is_category() || is_archive() || is_post_type_archive())
{
	$catpage = '';
	
	if(get_query_var('paged'))
		$catpage = get_query_var('paged');
	elseif(get_query_var('page'))
		$catpage = get_query_var('page');
	
	$term_id =  get_queried_object_id();
	
	$cnt = countPostByTermId($term_id);
	
	$catChain = array();
	
	$catData = get_category($term_id, ARRAY_A);
	$catChain[$catData['term_id']] = $catData['slug'];
	
	if($catData['parent'])
	{
		$secondCatData = get_category($catData['parent'], ARRAY_A);
		$catChain[$secondCatData['term_id']] = $secondCatData['slug'];
		
		if($secondCatData['parent'])
		{
			$thirdCatData = get_category($secondCatData['parent'], ARRAY_A);
			$catChain[$thirdCatData['term_id']] = $thirdCatData['slug'];
		}
	}
	
	$catChain = array_reverse($catChain);
	
	$nextText = '';
	$prevText = '';
	
	if(($catpage == 1 || empty($catpage)) && $cnt > 30)
	{
		$nextText = '<link rel="next" href="https://beavercrafttools.com/category/'.implode('/',$catChain).'/?page=2">';
	}
	elseif(($catpage < ceil($cnt/30)) && $catpage > 1)
	{
		$nextText = '<link rel="next" href="https://beavercrafttools.com/category/'.implode('/',$catChain).'/?page='.($catpage+1).'">';
		
		if($catpage == 2)
			$prevTail = '';
		else
			$prevTail = '?page='.($catpage-1);
		$prevText = '<link rel="prev" href="https://beavercrafttools.com/category/'.implode('/',$catChain).'/'.$prevTail.'">';
	}
	elseif($catpage == ceil($cnt/30))
	{
		if($catpage == 2)
			$prevTail = '';
		else
			$prevTail = '?page='.($catpage-1);
		$prevText = '<link rel="prev" href="https://beavercrafttools.com/category/'.implode('/',$catChain).'/'.$prevTail.'">';
	}
	
	echo $nextText;
	echo $prevText;
}

?>

<?php consulting_thinkup_hook_header(); ?>

<meta charset="<?php bloginfo( 'charset' ); ?>" />

<meta name="viewport" content="width=device-width" />

<link rel="profile" href="//gmpg.org/xfn/11" />

<link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>" />

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600,600i,700,700i,900&amp;subset=cyrillic" rel="stylesheet">

<?php wp_head(); ?>

<!-- <msdropdown> -->
<!--<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/country-dropdown/css/msdropdown/dd.css" />
<script src="<?php echo get_template_directory_uri(); ?>/country-dropdown/js/msdropdown/jquery.dd.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/country-dropdown/css/msdropdown/flags.css" />-->



<!-- ��� �������� ����������� ��� Bing -->
<meta name="msvalidate.01" content="BCFB80A99DD6CD83C9B8E9DF0EEFFC82" />
<!-- ��� �������� ����������� ��� Bing -->

<!--��� ��� ���������-->
<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/5fb3de1017f1d0ce5d48b5fe4/303566e9092840ca50e8dded2.js");</script>
<!--��� ��� ���������-->

<!--Pinterest-->
<meta name="p:domain_verify" content="9ecf654e35efe575d90333f2768284b1"/>
<!--Pinterest-->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2088074194818464');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=2088074194818464&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Google Tag Manager RZ -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NFFTVLR');</script>
<!-- End Google Tag Manager RZ -->

<!-- Subscription form -->
<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us19.list-manage.com","uuid":"5fb3de1017f1d0ce5d48b5fe4","lid":"57a550e79b","uniqueMethods":true}) })</script>
<!-- Subscription form -->

</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) RZ -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NFFTVLR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) RZ -->

<div id="body-core" class="hfeed site">

	<header>	
	
	<div id="site-header">		
	
	<?php if ( get_header_image() ) : ?>			
	
	<div class="custom-header">
	
	<img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="">
	
	</div>		
	
	<?php endif; // End header image check. ?>			
	
	<div id="pre-header">		
	
	<div class="wrap-safari">		
	
	<div id="pre-header-core" class="main-navigation">			
	
	<?php /* Social Media Icons */ consulting_thinkup_input_socialmediaheaderpre(); ?>			
	
	<?php if ( has_nav_menu( 'pre_header_menu' ) ) : ?>			
	
	<?php wp_nav_menu( array( 'container_class' => 'header-links', 'container_id' => 'pre-header-links-inner', 'theme_location' => 'pre_header_menu' ) ); ?>			
	
	<?php endif; ?>		
	
	</div>		
	
	</div>		
	
	</div>		
	
	<!-- #pre-header -->		
	
	<div id="header">		
	
	<div id="header-core">			
	
	<div id="logo">			
	
	<?php /* Custom Logo */ echo consulting_thinkup_custom_logo(); ?>			
	
	</div>			
	
	<div id="header-links" class="main-navigation">			
	
	<div id="header-links-inner" class="header-links">				
	
	<?php 
	
	$walker = new consulting_thinkup_menudescription;				
	
	wp_nav_menu(array( 'container' => false, 'theme_location'  => 'header_menu', 'walker' => new consulting_thinkup_menudescription() ) ); ?>

	<?php consulting_thinkup_input_headersearch(); ?>
	<div id="dincart"><?php //echo do_shortcode('[wp_compact_cart]'); ?></div>	
	
	</div>			
	
	</div>			
	
	<!-- #header-links .main-navigation --> 				
	
	<?php /* Add responsive header menu */ consulting_thinkup_input_responsivehtml1(); ?>		
	
	</div>		
	
	</div>		
	
	<!-- #header -->		
	
	<?php /* Add responsive header menu */ consulting_thinkup_input_responsivehtml2(); ?>		
	
	<?php /* Add sticky header */ consulting_thinkup_input_headersticky(); ?>		
	
	<?php /* Custom Slider */ 
	
	if(!isset($_GET['single_prod_id']))
	{
		
	consulting_thinkup_input_sliderhome(); 
	
	}
	?>		
	
	<!-- < ?php /* Custom Intro - Above */ consulting_thinkup_custom_intro(); ?> -->	
	
	</div>	
	
	</header>	
	
	<!-- header -->			
	
	<div id="content">	
	
	<div id="content-core">		
	
	<div id="main">		
	
	<div id="main-core">