<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package ThinkUpThemes
 <i class="fa fa-ban"></i>
 */

get_header(); ?>

	<div class="entry-content title-404">
		<h2><?php pll_e( '404' ); ?></h2>
		<p> </p>
		<h3><?php pll_e( 'page_not_found' ); ?><br/><?php pll_e( 'press_back_button' ); ?></h3>
		<p> </p>
		<p> </p>
		<a href="http://beavercrafttools.com" >
		<button><?php pll_e( '404_button_text' ); ?></button>
		</a>
		
		<p></p>
	</div>

<?php get_footer(); ?>