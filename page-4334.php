<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 *
 * @package ThinkUpThemes
 */

get_header(); 
global $wpdb;

$posts = get_posts('post_type=product&post_status=publish&showposts=-1');

foreach($posts as $post)
{
	$ms_slug = get_field('ms_slug', $post->ID);
	
	if(trim($ms_slug) != '')
	{
		$code1 = $wpdb->get_results('SELECT code1 FROM tempms WHERE article="'.$ms_slug.'"', ARRAY_A);
		
		$code1 = $code1[0]['code1'];
		
		update_post_meta($post->ID, 'ms_id', $code1);
	}
}
 get_footer(); ?>