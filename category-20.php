<?php
/**
 * The template for displaying News.
 *
 * @package ThinkUpThemes
 */

get_header(); 


$currentCatID = get_query_var( 'cat' );
 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
 
	$the_query = new WP_Query( array( 'category__in' => array($currentCatID), 'post_type' => 'post', 'posts_per_page' => 30, 'paged' => $paged) );

	// Цикл WordPress
	if( $the_query->have_posts() ){ 
		  while( $the_query->have_posts() ){ 
			  $the_query->the_post();
			   ?>
			   <div class="blog-grid element<?php consulting_thinkup_input_stylelayout(); ?>">					
		 
			 <article id="post-<?php the_ID(); ?>" <?php post_class('blog-article'); ?>>						
			 
				 <?php if( has_post_thumbnail() ) { ?>						
				 
					 <header class="entry-header">							
					 
						<?php echo consulting_thinkup_input_blogimage(); ?>									
					 
					 </header>						
				 
				 <?php } ?>						
				 
				 <div class="entry-content">							
				 
					 <?php consulting_thinkup_input_blogtitle(); ?>													
					 						
				 
				 </div>
				 
				 <div class="clearboth"></div>					
			 
			 </article>
			 
			 <!-- #post-<?php get_the_ID(); ?> -->					
		 
		 </div>				
						<?php	  
						  }
						  wp_reset_query();
					} else {
					  ?>
					  Category is empty
					  <?php
					}
					
					 ?>
					 <div class="clearboth"></div>
					 <div class="paging">
					 <?php		
					$big = 999999999; // уникальное число
					echo paginate_links( array(
						'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format'  => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total'   => $the_query->max_num_pages
					) );
					
				?>
				</div>
<?php get_footer() ?>