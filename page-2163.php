<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 *
 * @package ThinkUpThemes
 */

get_header(); 
if(isset($_SESSION['simpleCart']))
	unset($_SESSION['simpleCart']);
if(isset($_SESSION['order_id']))
	unset($_SESSION['order_id']);
?>
<div style="padding-top:175px; padding-bottom:200px;">
	<center>
		<img style="width:260px; height:260px;" src="<?php echo get_template_directory_uri(); ?>/can.png">
		<h1>Payment cancelled</h1>
	</center>
</div>
<?php

get_footer(); ?>